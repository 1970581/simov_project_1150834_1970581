package simov.isep.ondeandas.persistencia;

import android.content.Context;

public class AjudaPersistencia {

    public static IPersistencia getDB(Context context){
        // BD em Memoria
        //return new MemoriaPersistencia();
        // BD SQL
        return new DBAdapter(context);
    }
}
