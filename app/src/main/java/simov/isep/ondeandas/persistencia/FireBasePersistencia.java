package simov.isep.ondeandas.persistencia;

import java.util.List;

import simov.isep.ondeandas.LocalListAdapter;
import simov.isep.ondeandas.model.Local;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FireBasePersistencia /**implements IPersistencia*/ {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference ref = database.getReference("locais");

    //@Override
    public Local getLocalById(int id) {
        return null;
    }

    //@Override
    public boolean criarLocal(Local novoLocal) {
        String dbID = ref.push().getKey();
        ref.child(dbID).setValue(novoLocal);
        return true;
    }

    //@Override
    public boolean editarLocal(Local localAEditar) {
        return false;
    }

    //@Override
    public boolean apagarLocal(Local localAApagar) {
        return false;
    }

    //@Override
    public void getAllLocal(final LocalListAdapter adapter,final List<Local> localList) {
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                localList.clear();
                for (DataSnapshot localData : dataSnapshot.getChildren()) {
                    Local local = localData.getValue(Local.class);
                    localList.add(local);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
