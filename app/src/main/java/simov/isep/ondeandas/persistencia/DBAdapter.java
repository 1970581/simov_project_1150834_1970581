package simov.isep.ondeandas.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import simov.isep.ondeandas.model.Horario;
import simov.isep.ondeandas.model.Local;

public class DBAdapter implements IPersistencia{
    private DBHelper dbHelper;

    public DBAdapter(Context context) {
        dbHelper = new DBHelper(context);
        if(getAllLocal().size() == 0 && getAllHorario().size() == 0) {
            Local a = new Local();
            a.setId(1);
            a.setNome("Hugo");
            //41.189445, -8.702840
            a.setLat(41.189445);
            a.setLng(-8.702840);
            a.setRaio(100);

            Local b = new Local();
            b.setId(2);
            b.setNome("ISEP B409");
            //41.176945,-8.608080 DO prof.
            //41.177684, -8.607788 do ISEP b309
            b.setLat(41.177684);
            b.setLng(-8.607788);
            b.setRaio(100);

            Local c = new Local();
            c.setId(3);
            c.setNome("Tiago");

            //41.164189 -8.552041 Local do Tiago
            c.setLat(41.164189);
            c.setLng(-8.552041);
            c.setRaio(100);
            criarLocal(a);
            criarLocal(b);
            criarLocal(c);

            Horario h1 = new Horario();
            Horario h2 = new Horario();
            Horario h3 = new Horario();

            Horario h;

            h = h1;
            h.setDiaSemana(1);h.setNome("Domingo");
            h.setId(1);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);


            h = h2;
            h.setDiaSemana(2);h.setNome("Segunda");
            h.setId(2);h.setIdLocal(2);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            h = h3;
            h.setDiaSemana(3);h.setNome("Terca");
            h.setId(2);h.setIdLocal(3);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            criarHorario(h1);
            criarHorario(h2);
            criarHorario(h3);
        }
    }

    @Override
    public Local getLocalById(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + dbHelper.getTableLocal() + " WHERE "
                + dbHelper.getKeyId() + " = " + id;

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null && c.moveToFirst()) {

            Local local = new Local();
            local.setId(c.getInt(c.getColumnIndex(dbHelper.getKeyId())));
            local.setNome((c.getString(c.getColumnIndex(dbHelper.getLocalNome()))));
            local.setLat(c.getDouble(c.getColumnIndex(dbHelper.getLocalLat())));
            local.setLng(c.getDouble(c.getColumnIndex(dbHelper.getLocalLng())));
            local.setRaio(c.getInt(c.getColumnIndex(dbHelper.getLocalRaio())));

            c.close();
            db.close();

            return local;
        }

        db.close();
        return null;
    }

    @Override
    public boolean criarLocal(Local novoLocal) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            //values.put(dbHelper.getKeyId(), novoLocal.getId());
            values.put(dbHelper.getLocalNome(), novoLocal.getNome());
            values.put(dbHelper.getLocalLat(), novoLocal.getLat());
            values.put(dbHelper.getLocalLng(), novoLocal.getLng());
            values.put(dbHelper.getLocalRaio(), novoLocal.getRaio());

            db.insert(dbHelper.getTableLocal(), null, values);

            db.close();

            return true;
        } catch (SQLException sqlerror) {
            Log.v("Insert into table error", sqlerror.getMessage());
            return false;
        }
    }

    @Override
    public boolean editarLocal(Local localAEditar) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(dbHelper.getLocalNome(), localAEditar.getNome());
            values.put(dbHelper.getLocalLat(), localAEditar.getLat());
            values.put(dbHelper.getLocalLng(), localAEditar.getLng());
            values.put(dbHelper.getLocalRaio(), localAEditar.getRaio());

            db.update(dbHelper.getTableLocal(), values, dbHelper.getKeyId() + " = ?",
                    new String[]{String.valueOf(localAEditar.getId())});
            db.close();

            return true;
        } catch (SQLException sqlerror) {
            Log.v("Update into table error", sqlerror.getMessage());
            return false;
        }
    }

    @Override
    public boolean apgarLocal(Local localAApagar) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            db.delete(dbHelper.getTableLocal(), dbHelper.getKeyId() + " = ?",
                    new String[]{String.valueOf(localAApagar.getId())});
            db.close();

            return true;
        } catch (SQLException sqlerror) {
            Log.v("Delete into table error", sqlerror.getMessage());
            return false;
        }
    }

    @Override
    public List<Local> getAllLocal() {
        List<Local> locais = new ArrayList<Local>();
        String selectQuery = "SELECT  * FROM " + dbHelper.getTableLocal();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null && c.moveToFirst()) {
            do {
                Local local = new Local();
                local.setId(c.getInt(c.getColumnIndex(dbHelper.getKeyId())));
                local.setNome((c.getString(c.getColumnIndex(dbHelper.getLocalNome()))));
                local.setLat(c.getDouble(c.getColumnIndex(dbHelper.getLocalLat())));
                local.setLng(c.getDouble(c.getColumnIndex(dbHelper.getLocalLng())));
                local.setRaio(c.getInt(c.getColumnIndex(dbHelper.getLocalRaio())));

                locais.add(local);
            } while (c.moveToNext());
            c.close();
        }

        db.close();

        return locais;
    }

    @Override
    public Horario getHorarioById(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + dbHelper.getTableHorario() + " WHERE "
                + dbHelper.getKeyId() + " = " + id;

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null && c.moveToFirst()) {

            Horario horario = new Horario();
            horario.setId(c.getInt(c.getColumnIndex(dbHelper.getKeyId())));
            horario.setNome((c.getString(c.getColumnIndex(dbHelper.getHorarioNome()))));
            horario.setDiaSemana((c.getInt(c.getColumnIndex(dbHelper.getHorarioDiaSemana()))));
            horario.setHoraInicio((c.getInt(c.getColumnIndex(dbHelper.getHorarioHoraInicio()))));
            horario.setMinutoInicio(c.getInt(c.getColumnIndex(dbHelper.getHorarioMinutoInicio())));
            horario.setHoraFim((c.getInt(c.getColumnIndex(dbHelper.getHorarioHoraFim()))));
            horario.setMinutoFim((c.getInt(c.getColumnIndex(dbHelper.getHorarioMinutoFim()))));
            horario.setIdLocal((c.getInt(c.getColumnIndex(dbHelper.getHorarioIdLocal()))));

            c.close();
            db.close();

            return horario;
        }

        db.close();
        return  null;
    }

    @Override
    public boolean criarHorario(Horario novoHorario) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            //values.put(dbHelper.getKeyId(), novoHorario.getId());
            values.put(dbHelper.getLocalNome(), novoHorario.getNome());
            values.put(dbHelper.getHorarioDiaSemana(), novoHorario.getDiaSemana());
            values.put(dbHelper.getHorarioHoraInicio(), novoHorario.getHoraInicio());
            values.put(dbHelper.getHorarioMinutoInicio(), novoHorario.getMinutoInicio());
            values.put(dbHelper.getHorarioHoraFim(), novoHorario.getHoraFim());
            values.put(dbHelper.getHorarioMinutoFim(), novoHorario.getMinutoFim());
            values.put(dbHelper.getHorarioIdLocal(), novoHorario.getIdLocal());

            db.insert(dbHelper.getTableHorario(), null, values);

            db.close();

            return true;
        } catch (SQLException sqlerror) {
            Log.v("Insert into table error", sqlerror.getMessage());
            return false;
        }
    }

    @Override
    public boolean editarHorario(Horario horarioAEditar) {
        try{
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(dbHelper.getLocalNome(), horarioAEditar.getNome());
            values.put(dbHelper.getHorarioDiaSemana(), horarioAEditar.getDiaSemana());
            values.put(dbHelper.getHorarioHoraInicio(), horarioAEditar.getHoraInicio());
            values.put(dbHelper.getHorarioMinutoInicio(), horarioAEditar.getMinutoInicio());
            values.put(dbHelper.getHorarioHoraFim(), horarioAEditar.getHoraFim());
            values.put(dbHelper.getHorarioMinutoFim(), horarioAEditar.getMinutoFim());
            values.put(dbHelper.getHorarioIdLocal(), horarioAEditar.getIdLocal());

            db.update(dbHelper.getTableHorario(), values, dbHelper.getKeyId() + " = ?",
                new String[] { String.valueOf(horarioAEditar.getId()) });

            db.close();

            return true;
        } catch (SQLException sqlerror) {
            Log.v("Update into table error", sqlerror.getMessage());
            return false;
        }
    }

    @Override
    public boolean apagarHorario(Horario horarioAApagar) {
        try{
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            db.delete(dbHelper.getTableHorario(), dbHelper.getKeyId() + " = ?",
                new String[] { String.valueOf(horarioAApagar.getId()) });

            db.close();

            return true;
        } catch (SQLException sqlerror) {
            Log.v("Delete into table error", sqlerror.getMessage());
            return false;
        }
    }

    @Override
    public List<Horario> getAllHorario() {
        List<Horario> horarios = new ArrayList<Horario>();
        String selectQuery = "SELECT  * FROM " + dbHelper.getTableHorario();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null && c.moveToFirst()) {
            do {
                Horario horario = new Horario();
                horario.setId(c.getInt(c.getColumnIndex(dbHelper.getKeyId())));
                horario.setNome((c.getString(c.getColumnIndex(dbHelper.getHorarioNome()))));
                horario.setDiaSemana((c.getInt(c.getColumnIndex(dbHelper.getHorarioDiaSemana()))));
                horario.setHoraInicio((c.getInt(c.getColumnIndex(dbHelper.getHorarioHoraInicio()))));
                horario.setMinutoInicio(c.getInt(c.getColumnIndex(dbHelper.getHorarioMinutoInicio())));
                horario.setHoraFim((c.getInt(c.getColumnIndex(dbHelper.getHorarioHoraFim()))));
                horario.setMinutoFim((c.getInt(c.getColumnIndex(dbHelper.getHorarioMinutoFim()))));
                horario.setIdLocal((c.getInt(c.getColumnIndex(dbHelper.getHorarioIdLocal()))));

                horarios.add(horario);
            } while (c.moveToNext());
            c.close();
        }

        db.close();
        return horarios;
    }
}
