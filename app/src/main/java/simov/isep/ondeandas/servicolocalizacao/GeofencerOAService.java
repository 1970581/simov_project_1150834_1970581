package simov.isep.ondeandas.servicolocalizacao;

import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

import simov.isep.ondeandas.LogicaUi;
import simov.isep.ondeandas.MainActivity;
import simov.isep.ondeandas.R;
import simov.isep.ondeandas.model.Local;
import simov.isep.ondeandas.persistencia.AjudaPersistencia;
import simov.isep.ondeandas.persistencia.IPersistencia;
import simov.isep.ondeandas.persistencia.MemoriaLogs;

public class GeofencerOAService extends Service {

    private static final String TAG = "GeofencerOAService";
    private static final String MY_SPACER = "****************************************************** ";

    //Broadcast action filters, para diferenciar as mensagens.
    public static final String INTENT_FILTER_ACTION_GEO_SERVICE_STARTED = "OA_GEO_SERVICE_STARTED";
    public static final String INTENT_FILTER_ACTION_GEO_SERVICE_STOPED = "OA_GEO_SERVICE_STOPED";


    public static GeofencingClient mGeofencingClient;
    private static List<Geofence> mGeofenceList;
    public static PendingIntent mGeofencePendingIntent;
    public static boolean isStarted = false;
    public boolean isGeofenceActive = false;

    private final IBinder binder = new MyBinder();

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return binder;

    }

    @Override
    public boolean onUnbind(Intent intent) {
        Toast.makeText(this, "MyService:onUnbind", Toast.LENGTH_LONG).show();
        Log.e(TAG, "onUnbind: Service Unbind");
        return super.onUnbind(intent);
    }

    public class MyBinder extends Binder {
        public GeofencerOAService getService() {
            return GeofencerOAService.this;
        }
    }


    public GeofencerOAService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startGeoService();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, MY_SPACER + " GeofencerOAService ============= DESTROYED =====");
        MemoriaLogs.addLog("GeofencerOAService === DESTROYED ===");
        super.onDestroy();

    }





    public void startGeoService(){

        if(isStarted) return;

        //Desativamos o servico para o pai.
        if(isPai()) {
            Log.e(TAG, MY_SPACER + "Servico GeoFence PAI DETECTADO. Servico nao iniciado.");
            MemoriaLogs.addLog("Servico GeoFence PAI DETECTADO. Servico nao iniciado.");
            Toast.makeText(this, "Servico GeoFence PAI DETECTADO. Servico nao iniciado.", Toast.LENGTH_LONG).show();
            return;
        }

        mGeofencingClient = LocationServices.getGeofencingClient(this);

        createGeofencesInList();

        if(mGeofenceList == null || mGeofenceList.isEmpty()) {

            String msgnofence = getApplicationContext().getString(R.string.nome_no_locations);//"Lista de Geofence vazia. Cria Locais.";
            Toast.makeText(this, msgnofence, Toast.LENGTH_LONG).show();
            MemoriaLogs.addLog(msgnofence);
            return;
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            String mesage002 = getApplicationContext().getString(R.string.nome_necessita_permicoes_localizacao);
            Toast.makeText(getApplicationContext(), mesage002, Toast.LENGTH_LONG).show();
            //return;
        }
        else {
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener( new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            // Geofences added
                            // ...
                            String mesg04 = getApplicationContext().getString(R.string.nome_geofences_added);
                            MemoriaLogs.addLog("Bound Geofences Added.");
                            Log.e(TAG, MY_SPACER + mesg04);
                            Toast.makeText(getApplicationContext(), mesg04, Toast.LENGTH_SHORT).show();
                            //setIsGeofenceActiveTrue();
                            isStarted = true;
                            Intent intent = new Intent();
                            intent.setAction(GeofencerOAService.INTENT_FILTER_ACTION_GEO_SERVICE_STARTED);
                            sendBroadcast(intent);
                        }
                    })
                    .addOnFailureListener( new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Failed to add geofences
                            // ...
                            MemoriaLogs.addLog("Bound Geofences Added FAILED!!!!.");
                            String mesg05 = getApplicationContext().getString(R.string.nome_geofences_added_fail);
                            Toast.makeText(getApplicationContext(), mesg05, Toast.LENGTH_SHORT).show();
                            //isGeofenceActive = false;
                            isStarted = false;
                            //setIsGeofenceActiveFalse();
                            Intent intent = new Intent();
                            intent.setAction(GeofencerOAService.INTENT_FILTER_ACTION_GEO_SERVICE_STOPED);
                            sendBroadcast(intent);
                        }
                    });

        }

    }

    private void createGeofencesInList(){
        mGeofenceList = new ArrayList<Geofence>();

        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        List<Local> myLocalList = bd.getAllLocal();

        for (Local l : myLocalList){
            try{
                mGeofenceList.add(new Geofence.Builder()
                        .setRequestId(" " + l.getId() + " " + l.getNome())
                        .setCircularRegion(l.getLat(), l.getLng(), l.getRaio())
                        .setExpirationDuration(ConstantesLocalizacao.tempoExpiracaoDuracaoDasGeoGences)//Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                        .setTransitionTypes(
                                Geofence.GEOFENCE_TRANSITION_ENTER |
                                        Geofence.GEOFENCE_TRANSITION_DWELL |
                                        Geofence.GEOFENCE_TRANSITION_EXIT)
                        .setLoiteringDelay(ConstantesLocalizacao.tempoParaDelayDeLoitering)
                        .build());
                Log.i(TAG, MY_SPACER + "GeoFence Criada");
            }
            catch(Exception ex){
                Log.e(TAG, MY_SPACER + "GeoFence Erro a criar " + ex.getMessage());
                MemoriaLogs.addLog("Create Fence error: " + ex.getMessage());
                throw ex;
            }
        }

    }


    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        //builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER |  GeofencingRequest.INITIAL_TRIGGER_EXIT);
        // Vamos so fazer trigger inicia se entrarmos ou ja nos encontrarmos no local.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    public void endGeoService(){
        isStarted = false;
        mGeofencingClient.removeGeofences(getGeofencePendingIntent())
                .addOnSuccessListener( new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences removed
                        // ...
                        //setIsGeofenceActiveFalse();
                        isStarted = false;
                        MemoriaLogs.addLog("Bound Geofences Removed.");
                        String mesg07 = getApplicationContext().getString(R.string.nome_geofence_removed);
                        Toast.makeText(getApplicationContext(), mesg07, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent();
                        intent.setAction(GeofencerOAService.INTENT_FILTER_ACTION_GEO_SERVICE_STOPED);
                        sendBroadcast(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to remove geofences
                        // ...
                        MemoriaLogs.addLog("Bound Geofences Removed FAILED!!!.");
                        String mesg08 = getApplicationContext().getString(R.string.nome_geofence_removed_fail);
                        Log.e(TAG, MY_SPACER + mesg08 );
                        Toast.makeText(getApplicationContext(), mesg08, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public boolean isPai(){
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        boolean isPaiRole = sharedPref.getBoolean(LogicaUi.SHARED_PREFERENCE_FIELD_ROLE_IS_PAI,false);
        return isPaiRole;
    }

}
