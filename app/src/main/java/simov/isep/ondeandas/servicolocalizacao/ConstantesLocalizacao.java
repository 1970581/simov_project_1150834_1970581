package simov.isep.ondeandas.servicolocalizacao;

public class ConstantesLocalizacao {


    public static boolean requiresGeofenceUpdate = false;

    // Usado durante a criacao de uma geofense para indicar o tempo de duracao da geofence em ms.
    //Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
    //new Geofence.Builder().setExpirationDuration(ConstantesLocalizacao.tempoExpiracaoDuracaoDasGeoGences)
    public static  final int tempoExpiracaoDuracaoDasGeoGences = 1000 * 60 * 24; // 24h.

    // Usado durante a criacao de uma geofence para indicar o tempo a partir do qual é considerado loitering em ms.
    //setLoiteringDelay(15000)
    //new Geofence.Builder().setLoiteringDelay(15000)
    public static final int tempoParaDelayDeLoitering = 1000 * 5 * 60; // 5 min

}
