package simov.isep.ondeandas.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "OndeAndasBD";

    private static final String TABLE_LOCAL = "locais";
    private static final String TABLE_HORARIO = "horarios";

    private static final String KEY_ID = "id";

    private static final String LOCAL_NOME = "nome";
    private static final String LOCAL_LAT = "lat";
    private static final String LOCAL_LNG = "lng";
    private static final String LOCAL_RAIO = "raio";

    private static final String HORARIO_NOME = "nome";
    private static final String HORARIO_DIA_SEMANA = "diaSemana";
    private static final String HORARIO_HORA_INICIO = "horaInicio";
    private static final String HORARIO_MINUTO_INICIO = "minutoInicio";
    private static final String HORARIO_HORA_FIM = "horaFim";
    private static final String HORARIO_MINUTO_FIM = "minutoFim";
    private static final String HORARIO_ID_LOCAL = "idLocal";

    private static final String CREATE_TABLE_LOCAL = "CREATE TABLE IF NOT EXISTS "
            + TABLE_LOCAL + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + LOCAL_NOME
            + " TEXT," + LOCAL_LAT + " DOUBLE," + LOCAL_LNG
            + " DOUBLE," + LOCAL_RAIO + " INTEGER" + ")";

    private static final String CREATE_TABLE_HORARIO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_HORARIO + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + HORARIO_NOME
            + " TEXT," + HORARIO_DIA_SEMANA + " INTEGER," + HORARIO_HORA_INICIO
            + " INTEGER," + HORARIO_MINUTO_INICIO + " INTEGER,"
            + HORARIO_HORA_FIM + " INTEGER," + HORARIO_MINUTO_FIM
            + " INTEGER," + HORARIO_ID_LOCAL + " INTEGER" + ")";


    private Context context;
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_LOCAL);
        db.execSQL(CREATE_TABLE_HORARIO);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HORARIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCAL);

        //onCreate(db);
    }

    public String getTableLocal() {
        return TABLE_LOCAL;
    }

    public String getCreateTableHorario() {
        return CREATE_TABLE_HORARIO;
    }

    public String getKeyId() {
        return KEY_ID;
    }

    public String getLocalNome() {
        return LOCAL_NOME;
    }

    public String getLocalLat() {
        return LOCAL_LAT;
    }

    public String getLocalLng() {
        return LOCAL_LNG;
    }

    public String getLocalRaio() {
        return LOCAL_RAIO;
    }

    public String getTableHorario() {
        return TABLE_HORARIO;
    }

    public String getHorarioNome() {
        return HORARIO_NOME;
    }

    public String getHorarioDiaSemana() {
        return HORARIO_DIA_SEMANA;
    }

    public String getHorarioHoraInicio() {
        return HORARIO_HORA_INICIO;
    }

    public String getHorarioMinutoInicio() {
        return HORARIO_MINUTO_INICIO;
    }

    public String getHorarioHoraFim() {
        return HORARIO_HORA_FIM;
    }

    public String getHorarioMinutoFim() {
        return HORARIO_MINUTO_FIM;
    }

    public String getHorarioIdLocal() {
        return HORARIO_ID_LOCAL;
    }
}
