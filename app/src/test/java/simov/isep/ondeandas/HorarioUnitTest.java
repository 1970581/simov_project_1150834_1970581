package simov.isep.ondeandas;

import org.junit.Test;

import simov.isep.ondeandas.model.Horario;

import static org.junit.Assert.*;

public class HorarioUnitTest {

    @Test
    public void resetToBlankTest(){


        int Id = -1;
        int diaSemana = 2;
        String nome = "";
        int horaInicio = 00;
        int horaFim = 23;
        int minutoInicio = 00;
        int minutoFim = 55;
        int idLocal = -1;

        Horario hor = new Horario();
        hor.setNome("aaa");
        hor.resetToBlank();

        assertEquals(Id,hor.getId());
        assertEquals(diaSemana,hor.getDiaSemana());
        assertEquals(nome,hor.getNome());
        assertEquals(horaInicio,hor.getHoraInicio());
        assertEquals(horaFim,hor.getHoraFim());
        assertEquals(minutoInicio,hor.getMinutoInicio());
        assertEquals(minutoFim,hor.getMinutoFim());
        assertEquals(idLocal,hor.getIdLocal());

    }

    @Test
    public void cloneTest(){
        int Id = -1;
        int diaSemana = 2;
        String nome = "aaa";
        int horaInicio = 00;
        int horaFim = 23;
        int minutoInicio = 00;
        int minutoFim = 55;
        int idLocal = -1;

        Horario hor = new Horario();
        hor.setNome(nome);
        hor.setId(Id);
        hor.setDiaSemana(diaSemana);
        hor.setHoraInicio(horaInicio);
        hor.setHoraFim(horaFim);
        hor.setMinutoInicio(minutoInicio);
        hor.setMinutoFim(minutoFim);
        hor.setIdLocal(idLocal);
        Horario clone = hor.clone();
        assertTrue("clone not null", clone != null );

        assertEquals(Id, clone.getId());
        assertEquals(diaSemana, clone.getDiaSemana());
        assertEquals(nome, clone.getNome());
        assertEquals(horaInicio, clone.getHoraInicio());
        assertEquals(horaFim, clone.getHoraFim());
        assertEquals(minutoInicio, clone.getMinutoInicio());
        assertEquals(minutoFim, clone.getMinutoFim());
        assertEquals(idLocal, clone.getIdLocal());
    }

    @Test
    public void getTempoTest(){

        int Id = -1;
        int diaSemana = 2;
        String nome = "aaa";
        int horaInicio = 00;
        int horaFim = 23;
        int minutoInicio = 00;
        int minutoFim = 55;
        int idLocal = -1;

        Horario hor = new Horario();
        hor.setNome(nome);
        hor.setId(Id);
        hor.setDiaSemana(diaSemana);
        hor.setHoraInicio(horaInicio);
        hor.setHoraFim(horaFim);
        hor.setMinutoInicio(minutoInicio);
        hor.setMinutoFim(minutoFim);
        hor.setIdLocal(idLocal);

        assertEquals("00:00", hor.getTempoInicio());
        assertEquals("23:55", hor.getTempoFim());



    }

    @Test
    public void validateTest(){

        Horario h = new Horario();

        assertFalse("null name",h.validateNome(null));
        assertFalse("nome vazio",h.validateNome(""));
        assertTrue("nome = nome",h.validateNome("nome"));

        String dia = "1";
        assertTrue("dia semana: "+dia, h.validateDiaSemana(dia));
        dia = null;
        assertFalse("dia semana: "+dia, h.validateDiaSemana(dia));
        dia = "8";
        assertFalse("dia semana: "+dia, h.validateDiaSemana(dia));
        dia = "8a";
        assertFalse("dia semana: "+dia, h.validateDiaSemana(dia));

        String hora = "1";
        assertTrue("hora: "+hora, h.validateHora(hora));
        hora = "1a";
        assertFalse("hora: "+hora, h.validateHora(hora));
        hora = null;
        assertFalse("hora: "+hora, h.validateHora(hora));
        hora = "-1";
        assertFalse("hora: "+hora, h.validateHora(hora));
        hora = "24";
        assertFalse("hora: "+hora, h.validateHora(hora));

        String minuto = "0";
        assertTrue("minuto: "+hora, h.validateMinute(minuto));
        minuto = "59";
        assertTrue("minuto: "+hora, h.validateMinute(minuto));
        minuto = "-1";
        assertFalse("minuto: "+hora, h.validateMinute(minuto));
        minuto = "60";
        assertFalse("minuto: "+hora, h.validateMinute(minuto));
        minuto = null;
        assertFalse("minuto: "+hora, h.validateMinute(minuto));

        h.setHoraInicio(0);h.setMinutoInicio(0);
        h.setHoraFim(0);h.setMinutoFim(0);
        assertFalse("fimMaiorQueInicio " + h.getTempoInicio() +  " to " + h.getTempoFim(), h.validateFimMaiorQueInicio());

        h.setHoraInicio(0);h.setMinutoInicio(0);
        h.setHoraFim(0);h.setMinutoFim(1);
        assertTrue("fimMaiorQueInicio " + h.getTempoInicio() +  " to " + h.getTempoFim(), h.validateFimMaiorQueInicio());

        h.setHoraInicio(0);h.setMinutoInicio(59);
        h.setHoraFim(1);h.setMinutoFim(0);
        assertTrue("fimMaiorQueInicio " + h.getTempoInicio() +  " to " + h.getTempoFim(), h.validateFimMaiorQueInicio());

        h.setHoraInicio(1);h.setMinutoInicio(0);
        h.setHoraFim(1);h.setMinutoFim(0);
        assertFalse("fimMaiorQueInicio " + h.getTempoInicio() +  " to " + h.getTempoFim(), h.validateFimMaiorQueInicio());

        h.setHoraInicio(1);h.setMinutoInicio(0);
        h.setHoraFim(1);h.setMinutoFim(1);
        assertTrue("fimMaiorQueInicio " + h.getTempoInicio() +  " to " + h.getTempoFim(), h.validateFimMaiorQueInicio());

        h.setHoraInicio(23);h.setMinutoInicio(58);
        h.setHoraFim(23);h.setMinutoFim(59);
        assertTrue("fimMaiorQueInicio " + h.getTempoInicio() +  " to " + h.getTempoFim(), h.validateFimMaiorQueInicio());

    }


    @Test
    public void aumentarEDiminuirTest(){
        int Id = -1;
        int diaSemana = 2;
        String nome = "aaa";
        int horaInicio = 00;
        int horaFim = 23;
        int minutoInicio = 00;
        int minutoFim = 55;
        int idLocal = -1;

        Horario hor = new Horario();
        hor.setNome(nome);
        hor.setId(Id);
        hor.setDiaSemana(diaSemana);
        hor.setHoraInicio(horaInicio);
        hor.setHoraFim(horaFim);
        hor.setMinutoInicio(minutoInicio);
        hor.setMinutoFim(minutoFim);
        hor.setIdLocal(idLocal);

        Horario h = hor;
        int inserted = 1;
        int expected = 1;
        h.setDiaSemana(inserted);
        assertTrue("diaSemana " + inserted + " expected: "+ expected, expected == h.getDiaSemana());

        inserted = 1;
        expected = 7;
        h.setDiaSemana(inserted);h.diminuirDiaSemana();
        assertTrue("diaSemana " + inserted + " expected: "+ expected, expected == h.getDiaSemana());

        inserted = 2;
        expected = 1;
        h.setDiaSemana(inserted);h.diminuirDiaSemana();
        assertTrue("diaSemana " + inserted + " expected: "+ expected, expected == h.getDiaSemana());

        inserted = 7;
        expected = 1;
        h.setDiaSemana(inserted);h.aumentarDiaSemana();
        assertTrue("diaSemana " + inserted + " expected: "+ expected, expected == h.getDiaSemana());

        inserted = 1;
        expected = 2;
        h.setDiaSemana(inserted);h.aumentarDiaSemana();
        assertTrue("diaSemana " + inserted + " expected: "+ expected, expected == h.getDiaSemana());

        inserted = 0;
        expected = 0;
        h.setHoraInicio(inserted);
        assertTrue("horaDeInicio " + inserted + " expected: "+ expected, expected == h.getHoraInicio());

        inserted = 0;
        expected = 1;
        h.setHoraInicio(inserted);h.aumentarHoraInicio();
        assertTrue("horaDeInicio " + inserted + " expected: "+ expected, expected == h.getHoraInicio());

        inserted = 23;
        expected = 0;
        h.setHoraInicio(inserted);h.aumentarHoraInicio();
        assertTrue("horaDeInicio " + inserted + " expected: "+ expected, expected == h.getHoraInicio());

        inserted = 0;
        expected = 23;
        h.setHoraInicio(inserted);h.diminuirHoraInicio();
        assertTrue("horaDeInicio " + inserted + " expected: "+ expected, expected == h.getHoraInicio());

        inserted = 2;
        expected = 1;
        h.setHoraInicio(inserted);h.diminuirHoraInicio();
        assertTrue("horaDeInicio " + inserted + " expected: "+ expected, expected == h.getHoraInicio());



        inserted = 0;
        expected = 1;
        h.setHoraFim(inserted);h.aumentarHoraFim();
        assertTrue("horaDeFim " + inserted + " expected: "+ expected, expected == h.getHoraFim());

        inserted = 23;
        expected = 0;
        h.setHoraFim(inserted);h.aumentarHoraFim();
        assertTrue("horaDeFim " + inserted + " expected: "+ expected, expected == h.getHoraFim());

        inserted = 0;
        expected = 23;
        h.setHoraFim(inserted);h.diminuirHoraFim();
        assertTrue("horaDeFim " + inserted + " expected: "+ expected, expected == h.getHoraFim());

        inserted = 2;
        expected = 1;
        h.setHoraFim(inserted);h.diminuirHoraFim();
        assertTrue("horaDeFim " + inserted + " expected: "+ expected, expected == h.getHoraFim());


        inserted = 0;
        expected = 5;
        h.setMinutoInicio(inserted);h.aumentarMinutoInicio();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoInicio());

        inserted = 59;
        expected = 0;
        h.setMinutoInicio(inserted);h.aumentarMinutoInicio();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoInicio());

        inserted = 5;
        expected = 0;
        h.setMinutoInicio(inserted);h.diminuirMinutoInicio();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoInicio());

        inserted = 0;
        expected = 55;
        h.setMinutoFim(inserted);h.diminuirMinutoFim();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoFim());

        inserted = 0;
        expected = 5;
        h.setMinutoFim(inserted);h.aumentarMinutoFim();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoFim());

        inserted = 59;
        expected = 0;
        h.setMinutoFim(inserted);h.aumentarMinutoFim();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoFim());

        inserted = 5;
        expected = 0;
        h.setMinutoFim(inserted);h.diminuirMinutoFim();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoFim());

        inserted = 0;
        expected = 55;
        h.setMinutoFim(inserted);h.diminuirMinutoInicio();
        assertTrue("minutoDeInicio " + inserted + " expected: "+ expected, expected == h.getMinutoInicio());


    }

    @Test
    public void compareToTest(){

        int Id = -1;
        int diaSemana = 2;
        String nome = "aaa";
        int horaInicio = 00;
        int horaFim = 23;
        int minutoInicio = 00;
        int minutoFim = 55;
        int idLocal = -1;

        Horario hor = new Horario();
        hor.setNome(nome);
        hor.setId(Id);
        hor.setDiaSemana(diaSemana);
        hor.setHoraInicio(horaInicio);
        hor.setHoraFim(horaFim);
        hor.setMinutoInicio(minutoInicio);
        hor.setMinutoFim(minutoFim);
        hor.setIdLocal(idLocal);

        Horario h1 = hor.clone();
        Horario h2 = hor.clone();

        assertTrue("iguais", h1.compareTo(h2) == 0);

        h1.setId(1);h2.setId(2);
        assertTrue("id 1 2", h1.compareTo(h2) > 0);
        h1.setId(2);h2.setId(1);
        assertTrue("id 2 1", h1.compareTo(h2) < 0);
        h1.setId(0);h2.setId(0);

        h1.setDiaSemana(1);h2.setDiaSemana(2);
        assertTrue("dia 1 2", h1.compareTo(h2) < 0);
        h1.setDiaSemana(2);h2.setDiaSemana(1);
        assertTrue("dia 2 1", h1.compareTo(h2) > 0);
        h1.setDiaSemana(1);h2.setDiaSemana(1);

        h1.setHoraInicio(1);h2.setHoraInicio(2);
        assertTrue("hora 1 2", h1.compareTo(h2) < 0);

        h1.setHoraInicio(2);h2.setHoraInicio(1);
        assertTrue("hora 2 1", h1.compareTo(h2) > 0);


    }



}
