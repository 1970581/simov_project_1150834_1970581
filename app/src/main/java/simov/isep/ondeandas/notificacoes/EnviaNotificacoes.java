package simov.isep.ondeandas.notificacoes;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import simov.isep.ondeandas.persistencia.MemoriaLogs;

public class EnviaNotificacoes extends AsyncTask<String,Integer,Boolean> {

    private static final String TAG = "EnviaNotificacoes";

    private static final String SURL = "https://fcm.googleapis.com/fcm/send";
    private static String authKey = "key=AAAAluc2L6Y:APA91bFQ6ggE9qYE36h--SdUyyngX12Vv6xEDNLK5arDMApCSIuTbPm8ExSprDv68edbwZbzjP4l5UEu5HVItcuONEacGZnDimlVhbnKNerBHXXDXzdxKP-4Q9uA0_aG3mnJ-htB8fPZ";

    @Override
    protected Boolean doInBackground(String... params) {
        try {
            sendNotificacao(params);
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public void sendNotificacao(String[] texto) {

        if(texto == null ) return;
        String oBody = "erro";
        try{
            oBody = texto[0];
        }
        catch(Exception e){
            Log.e(TAG, TAG + " oBody = texto[0]; excecao: " + e.getMessage());
        }


        try {
            JSONObject jsonMain = new JSONObject();
            jsonMain.put("to", "/topics/ondeAndas");

            JSONObject jsonNotification = new JSONObject();

            jsonNotification.put("title", "Onde Andas");
            //jsonNotification.put("body", "Filho fora de local");
            jsonNotification.put("body", oBody);
            jsonNotification.put("content_available", true);
            jsonNotification.put("priority", "high");
            jsonMain.put("notification", jsonNotification);

            JSONObject jsonData = new JSONObject();
            jsonData.put("title", "Onde Andas");
            //jsonData.put("body", "Filho fora de local");
            jsonData.put("body", oBody);
            jsonData.put("content_available", true);
            jsonData.put("priority", "high");
            jsonMain.put("data", jsonData);

            String sendJsonData = jsonMain.toString();

            URL url = new URL(SURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setDoOutput(true);

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", authKey);

            conn.connect();
            OutputStream os = conn.getOutputStream();
            OutputStreamWriter wr = new OutputStreamWriter(os, "UTF-8");
            wr.write(sendJsonData);
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
            System.out.println("POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                System.out.println("succeeded");
            }

            conn.disconnect();
        } catch (Exception e) {
            Log.e("Exception", e.toString());
            MemoriaLogs.addLog(TAG + " " + e.getMessage());
        }
    }
}
