package simov.isep.ondeandas.servicolocalizacao;

import android.Manifest;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import simov.isep.ondeandas.persistencia.MemoriaLogs;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SendSMSIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "simov.isep.ondeandas.servicolocalizacao.action.FOO";
    private static final String ACTION_BAZ = "simov.isep.ondeandas.servicolocalizacao.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "simov.isep.ondeandas.servicolocalizacao.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "simov.isep.ondeandas.servicolocalizacao.extra.PARAM2";

    public SendSMSIntentService() {
        super("SendSMSIntentService");
    }

    public static final String INTENT_EXTRA_STRING_BODY_FIELD_OA = "INTENT_EXTRA_STRING_BODY_FIELD_OA" ;
    public static final String INTENT_EXTRA_STRING_NUMBER_FIELD_OA = "INTENT_EXTRA_INT_NUMBER_FIELD_OA" ;

    static final String TAG = "GeofenceTransitionsIntS";
    private static final String MY_PAD = "***************************************************** ";


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            // Confirmar os conteudos do intent.
            if(!intent.hasExtra(INTENT_EXTRA_STRING_BODY_FIELD_OA) || !intent.hasExtra(INTENT_EXTRA_STRING_NUMBER_FIELD_OA) ){
                Log.e(TAG, MY_PAD + TAG + " Intent received without both extras.");
                return;
            }
            String i_body = intent.getStringExtra(INTENT_EXTRA_STRING_BODY_FIELD_OA);
            String i_numero = intent.getStringExtra(INTENT_EXTRA_STRING_NUMBER_FIELD_OA);

            //Confirmar permissoes
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS) !=
                    PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, MY_PAD + TAG + " Sem permicoes.");
                return;
            }

            // Emviar o SMS
            sendSMS(i_numero,i_body);
        }
    }

    public void sendSMS(String sms_number, String sms_body ){

        Thread myNewThread = new MySMSThread(sms_number, sms_body);
        new Handler(Looper.getMainLooper()).post(myNewThread);

    }

    // CLASSE semi privada para enviar toasts. Temos de enviar na outra thread pk a thread to intent morre
    class MySMSThread extends Thread {

        public String theNumber = "0";
        public String theBody = "error";
        public MySMSThread(String myNumber,String myMsgeBody){
            super();
            this.theBody = myMsgeBody;
            this.theNumber = myNumber;
        }

        @Override
        public void run(){
            String text_log = "SMS_IService " + theNumber + " " + theBody;
            Log.e(TAG, MY_PAD + " sendSMS: "+ text_log);


            String destinationAddress = theNumber;
            String smsMessage = theBody;


            // Set the service center address if needed, otherwise null.
            String scAddress = null;
            // Set pending intents to broadcast
            // when message sent and when delivered, or set to null.
            PendingIntent sentIntent = null, deliveryIntent = null;

            // Use SmsManager.
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage
                    (destinationAddress, scAddress, smsMessage,
                            sentIntent, deliveryIntent);


            MemoriaLogs.addLog(text_log);
            Toast.makeText(getApplicationContext(), text_log, Toast.LENGTH_LONG).show();

        }
    }

}
