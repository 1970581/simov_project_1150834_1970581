package simov.isep.ondeandas.servicolocalizacao;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.google.android.gms.location.GeofencingEvent;

import simov.isep.ondeandas.LogicaUi;
import simov.isep.ondeandas.R;
import simov.isep.ondeandas.notificacoes.EnviaNotificacoes;
import simov.isep.ondeandas.persistencia.MemoriaLogs;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GeofenceTransitionsIntentService extends IntentService {

    private static final String TAG = "GeofenceTransitionsIntS";
    private static final String MY_PAD = "***************************************************** ";

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }





    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {

            //String errorMessage = GeofenceErrorMessages.getErrorString(this, geofencingEvent.getErrorCode());
            String errorMessage = "GeofenceErrorMessages error code " + geofencingEvent.getErrorCode();
            //Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
            //sendNotification(errorMessage);
            MemoriaLogs.addLog(errorMessage);
            Log.e(TAG, errorMessage);
            return;
        }
        Log.e(TAG, MY_PAD + " EVENTO FORA**********");

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition ==     Geofence.GEOFENCE_TRANSITION_DWELL ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            Log.e(TAG, MY_PAD + " EVENTO  DENTRO**********");

            // Get the geofences that were triggered. A single event can trigger multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(
                    this,
                    geofenceTransition,
                    triggeringGeofences
            );

            // Send notification and log the transition details.
            sendNotification(geofenceTransitionDetails);
            Log.i(TAG, geofenceTransitionDetails);
        } else {
            // Log the error.
            Log.e(TAG, MY_PAD + " getString(R.string.geofence_transition_invalid_type, geofenceTransition): " + geofenceTransition);
        }


    }

    public String getGeofenceTransitionDetails(Object alg, int geoftrans, List<Geofence> lista){
        String nomes = "";
        String transicao;
        switch (geoftrans)
        {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                //transicao = "TRANSITION_ENTER";
                transicao = getApplicationContext().getString(R.string.nome_TRANSITION_ENTER);
                break;
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                //transicao = "TRANSITION_DWELL";
                transicao = getApplicationContext().getString(R.string.nome_TRANSITION_DWELL);
                break;
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                //transicao = "TRANSITION_EXIT";
                transicao = getApplicationContext().getString(R.string.nome_TRANSITION_EXIT);
                break;
            default: transicao = "TRANSITION_UNKNOWN";
                break;
        }

        for(Geofence g : lista){
            nomes = nomes + " " +  g.getRequestId();
        }

        String resp = "" + transicao + " " + nomes;

        return resp;
    }


    // Metodo para enviar um Toast atravez de uma outra Thread.
    public void sendNotification(String texto){


        Thread myNewThread = new MyThread(texto);
        new Handler(Looper.getMainLooper()).post(myNewThread);

        //Envio da notificacao. **************************************************************************************
        //DateFormat df = new SimpleDateFormat("yyyy.MM.dd, HH:mm");
        //String date = df.format(Calendar.getInstance().getTime());
        //new EnviaNotificacoes().execute(date + " "+ toastMsg);
        //new EnviaNotificacoes().execute();


    }

    // CLASSE semi privada para enviar toasts. Temos de enviar na outra thread pk a thread to intent morre
    class MyThread extends Thread {

        public String toastMsg = "empty";
        public MyThread(String myMsge){
            super();
            this.toastMsg = myMsge;
        }
        @Override
        public void run(){

            String date = " ";
            String readMyUsername = "  ";

            DateFormat df = new SimpleDateFormat("yyyy.MM.dd, HH:mm");
            date = df.format(Calendar.getInstance().getTime());
            SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
            readMyUsername = sharedPref.getString(LogicaUi.SHARED_PREFERENCE_FIELD_USERNAME,"???");

            String nameAndPlaceTxt = readMyUsername + " " + toastMsg;
            String dataAndNameAndPlace = date + " " + readMyUsername + " " + toastMsg;
            MemoriaLogs.addLog(nameAndPlaceTxt);
            Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_LONG).show();

            //Envio da notificacao. *************************************************************************************
            new EnviaNotificacoes().execute(dataAndNameAndPlace);

            Log.e(TAG, MY_PAD + " Notificacao tentada enviar. Data: " + dataAndNameAndPlace);
        }
    }

}
