package simov.isep.ondeandas;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.Manifest;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.*;

// Parte do codigo e baseada na PL8 Location Updates, adaptado as necessidades da actividade.
public class ObtercoordenadasActivity extends AppCompatActivity {

    private static final String TAG = "ObtercoordenadasActivi";

    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1000;
    private GoogleApiClient googleApiClient;
    //private Location location;
    //private TextView tv;
    public EditText editText_lat;
    public EditText editText_lng;
    public Location myLocationNow;
    public Button button_voltar;

    static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    protected LocationRequest locationRequest;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationCallback locationListener = new LocationCallback(){

        @Override
        public void onLocationResult(LocationResult location) {
            Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":onLocationChanged");
            double lat01 = location.getLastLocation().getLatitude();
            double lng01 = location.getLastLocation().getLongitude();
            editText_lat.setText(String.valueOf(lat01));
            editText_lng.setText(String.valueOf(lng01));
            myLocationNow = location.getLastLocation();
            if(myLocationNow != null) button_voltar.setBackgroundColor(Color.GREEN);
            //tv.setText(tv.getText() + "Lat: \t" + String.valueOf(location.getLastLocation().getLatitude()) + "\nLng: \t" + String.valueOf(location.getLastLocation().getLongitude()) + "\nTm: \t" + String.valueOf(location.getLastLocation().getTime()) +"\n\n");
        }
    };

    private GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks(){

        @Override
        public void onConnected(Bundle bundle) {
            Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":onConnected");
            //Toast.makeText(getApplicationContext(), "Connected to Google Play Service", Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.nome_ligado_ao_google_play), Toast.LENGTH_LONG).show();
            if (ContextCompat.checkSelfPermission(ObtercoordenadasActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED
                    ||
                    ContextCompat.checkSelfPermission(ObtercoordenadasActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

                mFusedLocationClient.requestLocationUpdates(locationRequest, locationListener, Looper.myLooper());

                Log.d(TAG, "requestLocationUpdates");
            }
            else{
                Toast.makeText(getApplicationContext(), "No permission", Toast.LENGTH_LONG).show();
                if (googleApiClient.isConnected()) {
                    googleApiClient.disconnect();
                    Log.d(TAG, "no gps then disconnect apiclient");
                }
                handlePermissions();
            }
        }
        @Override
        public void onConnectionSuspended(int i) {
            Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":onConnectionSuspended");
            Toast.makeText(getApplicationContext(), "Connected to Google Play Services: Suspended", Toast.LENGTH_LONG).show();

            mFusedLocationClient.removeLocationUpdates(locationListener);
        }
    };

    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener(){

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":OnConnectionFailedListener");
            Toast.makeText(getApplicationContext(), "Connected to Google Play Services: Failed: error code: " + connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();

            mFusedLocationClient.removeLocationUpdates(locationListener);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obtercoordenadas);
        Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":onCreate");
        editText_lat = (EditText) findViewById(R.id.Obtercoordenadas_lat_edittext);
        editText_lng = (EditText) findViewById(R.id.Obtercoordenadas_lng_edittext);


        // CODIGO DO BOATAO DE VOLTAR
        button_voltar = (Button) findViewById(R.id.Obtercoordenadas_button_voltar);
        button_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(myLocationNow == null) return;

                Intent intent = new Intent();
                intent.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_LATITUDE_AS_DOUBLE, myLocationNow.getLatitude());
                intent.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_LONGITUDE_AS_DOUBLE, myLocationNow.getLongitude());
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });

        //tv = (TextView) findViewById(R.id.textView);

        if (isPlayServiceAvailable()) {
            buildGoogleApiClient();
            createLocationRequest();
        }else{
            Toast.makeText(this, "Google Play Service not Available", Toast.LENGTH_LONG).show();
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":onResume");
        if(googleApiClient != null) {
            if (!googleApiClient.isConnected()) {
                googleApiClient.connect();
                Log.d(TAG, "trying to connect");
            }
            Log.d(TAG, "googleApiClient not null");
        }
        else{ Log.d(TAG, "googleApiClient is null");}

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":onStop");
        if(googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                mFusedLocationClient.removeLocationUpdates(locationListener);
                googleApiClient.disconnect();
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "TID:" + Thread.currentThread().getId() + ":onDestroy");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_RECOVER_PLAY_SERVICES:
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Google Play Services must be installed.",Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient =  new GoogleApiClient.Builder(this,connectionCallbacks,connectionFailedListener)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean isPlayServiceAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
            }
            return false;
        }
        return true;
    }
    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED== ContextCompat.checkSelfPermission(this,perm));
    }

    private boolean handlePermissions() {
        Log.d(TAG, "test permissions");
        if(!hasPermission( Manifest.permission.ACCESS_FINE_LOCATION)
                || !hasPermission( Manifest.permission.ACCESS_COARSE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            return false;
        }
        return true;
    }


    public boolean validarLatLng(){
        return false;
    }

}
