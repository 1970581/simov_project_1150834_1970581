package simov.isep.ondeandas.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Local implements Parcelable {

    public void setId(int id) {
        Id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setRaio(int raio) {
        this.raio = raio;
    }

    public int Id;

    public String nome;

    public double lat;

    public double lng;

    public int raio;

    public final static int MAX_NAME_LENGHT = 60;
    public final static int MIN_RAIO = 1;


    public int getId(){return Id;}
    public String getNome(){return nome;}
    public double getLat() {return lat;}
    public double getLng() {return lng;}
    public int getRaio() {return raio;}

    public Local(){

    }
    protected Local(Parcel in) {
        this.Id = in.readInt();
        this.nome = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.raio = in.readInt();
    }

    // Validacoes!!!!

    /**
     * Serve para alterar um local com os dados das caixas de texto, strings.
     * @param nomeString
     * @param latString
     * @param lngString
     * @param raioInt
     * @return false se da erro, true se valida.
     */
    public boolean setDados(String nomeString, String latString, String lngString, String raioInt){

        if(!validateNome(nomeString)) return false;
        if(!validateLat(latString)) return false;
        if(!validateLng(lngString)) return false;
        if(!validateRaio(raioInt)) return false;

        double myLat = 0;
        double myLng = 0;
        int myRaio = 0;

        try{
            myLat = Double.parseDouble(latString);
            myLng = Double.parseDouble(lngString);
            myRaio = Integer.parseInt(raioInt);
        }
        catch (NumberFormatException ex){ return false; }

        this.nome = nomeString;
        this.lat = myLat;
        this.lng = myLng;
        this.raio = myRaio;

        return true;
    }

    public boolean validateNome(String nomeString){
        if(nomeString == null || nomeString.isEmpty() || nomeString.length() > MAX_NAME_LENGHT) return false;
        return true;
    }

    public boolean validateLat(String latString){
        if(latString == null || latString.isEmpty() ) return false;
        double myLat = 0;
        try{ myLat = Double.parseDouble(latString);  }
        catch (NumberFormatException ex){ return false; }
        return true;
    }

    public boolean validateLng(String lngString){
        if(lngString == null || lngString.isEmpty() ) return false;
        double myLat = 0;
        try{ myLat = Double.parseDouble(lngString);  }
        catch (NumberFormatException ex){ return false; }
        return true;
    }

    public boolean validateRaio(String raioInt){
        if(raioInt == null || raioInt.isEmpty()) return false;
        int myRaio = 0;
        try{ myRaio = Integer.parseInt(raioInt); }
        catch (NumberFormatException ex){ return false; }
        if(myRaio < MIN_RAIO) return false;
        return true;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Id);
        dest.writeString(this.nome);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
        dest.writeInt(this.raio);
    }


    public static final Creator<Local> CREATOR = new Creator<Local>() {
        @Override
        public Local createFromParcel(Parcel source) {
            return new Local(source);
        }

        @Override
        public Local[] newArray(int size) {
            return new Local[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Local local = (Local) o;

        return local.getId() == this.Id;
    }

    public Local clone(){
        Local clone = new Local();
        clone.Id = this.Id;
        clone.nome = this.nome;
        clone.lat = this.lat;
        clone.lng = this.lng;
        clone.raio = this.raio;
        return clone;
    }

}
