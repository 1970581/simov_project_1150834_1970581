package simov.isep.ondeandas.persistencia;

import java.util.List;

import simov.isep.ondeandas.model.Horario;
import simov.isep.ondeandas.model.Local;

public interface IPersistencia {

    public Local getLocalById(int id);

    public boolean criarLocal(Local novoLocal);

    public boolean editarLocal(Local localAEditar);

    public boolean apgarLocal(Local localAApagar);

    public List<Local> getAllLocal();

    public Horario getHorarioById(int id);

    public boolean criarHorario(Horario novoHorario);

    public boolean editarHorario(Horario horarioAEditar);

    public boolean apagarHorario(Horario horarioAApagar);

    public List<Horario> getAllHorario();
}
