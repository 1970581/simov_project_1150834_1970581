package simov.isep.ondeandas;


import org.junit.Test;

import simov.isep.ondeandas.model.Local;

import static org.junit.Assert.*;
public class LocalUnitTest {

    @Test
    public void addition_isCorrect2() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void setDadosTest(){

        boolean result;
        Local myLocal = new Local();
        String nome = "myLocal";
        String stringLat = "41.1";
        String stringLng = "-8.2";
        String stringRaio = "10";
        result = myLocal.setDados(nome, stringLat, stringLng, stringRaio);
        assertTrue("setDados ok",result);

        assertTrue("setDados ok",myLocal.getNome().equals(nome));
        assertTrue("setDados ok",myLocal.getLat() == 41.1);
        assertTrue("setDados ok",myLocal.getLng() == -8.2);
        assertTrue("setDados ok",myLocal.getRaio() == 10);


        result = myLocal.setDados(null, stringLat, stringLng, stringRaio);
        assertFalse("null name",result);

        result = myLocal.setDados(nome, null, stringLng, stringRaio);
        assertFalse("null lat",result);

        result = myLocal.setDados(nome, stringLat, null, stringRaio);
        assertFalse("null lng",result);

        result = myLocal.setDados(nome, stringLat, stringLng, null);
        assertFalse("null raio",result);

        result = myLocal.setDados(nome, stringLat, stringLng, "-1");
        assertFalse("raio negativo",result);

        result = myLocal.setDados(nome, stringLat, stringLng, "0");
        assertFalse("raio zero",result);



    }

    @Test
    public void cloneTest(){
        boolean result;
        Local myLocal = new Local();
        String nome = "myLocal";
        double lat = 41.1;
        double lng = -8.2;
        int raio = 10;
        int id = 0;
        myLocal.setNome(nome);
        myLocal.setId(id);
        myLocal.setLat(lat);
        myLocal.setLng(lng);
        myLocal.setRaio(raio);

        Local clone = myLocal.clone();

        assertTrue("clone not null", clone != null);

        assertEquals(id, clone.getId());
        assertEquals(nome, clone.getNome());
        assertEquals( lat, clone.getLat(), 0.1);
        assertEquals(lat, clone.getLat(), 0.1);
        assertEquals(raio, clone.getRaio());



    }

}
