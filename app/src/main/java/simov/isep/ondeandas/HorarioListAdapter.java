package simov.isep.ondeandas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import simov.isep.ondeandas.model.Horario;


public class HorarioListAdapter extends BaseAdapter {

    private final List<Horario> items;

    public HorarioListAdapter(final Context context, final int itemResId, final List<Horario> items) {
        this.items = items;
    }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int arg0) {
        return this.items.get(arg0);
    }

    public long getItemId(int arg0) {
        return 0;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        final Horario row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.horario_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row , AQUI COLOCOCAMOS AS CENAS NO horario_item.xml

        // ID:
        TextView txtId = (TextView) itemView.findViewById(R.id.horario_item_Id);
        txtId.setText(Integer.toString(row.getId()));

        // Nome:
        TextView txtTitle = (TextView) itemView.findViewById(R.id.horario_item_Nome);
        txtTitle.setText(row.getNome());

        // Dia
        TextView txtDia = (TextView) itemView.findViewById(R.id.horario_item_dia);
        Context context = arg2.getContext();

        int myDiaSemana = row.getDiaSemana();
        String myDya = "error";
        switch(myDiaSemana){
            case 1: myDya = context.getString(R.string.dia1); break;
            case 2: myDya = context.getString(R.string.dia2); break;
            case 3: myDya = context.getString(R.string.dia3); break;
            case 4: myDya = context.getString(R.string.dia4); break;
            case 5: myDya = context.getString(R.string.dia5); break;
            case 6: myDya = context.getString(R.string.dia6); break;
            case 7: myDya = context.getString(R.string.dia7); break;
            default: myDya = "ERROR"; break;
        }
        txtDia.setText(myDya);


        // Horas:
        TextView txtTimeInicio = (TextView) itemView.findViewById(R.id.horario_item_timeInicio);
        txtTimeInicio.setText(row.getTempoInicio());

        TextView txtTimeFim = (TextView) itemView.findViewById(R.id.horario_item_timeFim);
        txtTimeFim.setText(row.getTempoFim());

        return itemView;
    }

}
