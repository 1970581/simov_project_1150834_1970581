package simov.isep.ondeandas;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import simov.isep.ondeandas.model.Local;
import simov.isep.ondeandas.persistencia.AjudaPersistencia;
import simov.isep.ondeandas.persistencia.IPersistencia;
import simov.isep.ondeandas.persistencia.MemoriaPersistencia;

public class EscolherlocalActivity extends AppCompatActivity {

    //Actividade usada pela EditarHorarioActivity para escolher o local.
    private static final String TAG = "EscolherlocalActivity";

    static ListView localListView;
    ArrayList<Local> localList;
    private LocalListAdapter localListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolherlocal);

        // Lista de locais, e a sua View+Adapter
        localListView = (ListView) findViewById(R.id.Activity_escolherlocal_listView);
        localList = new ArrayList<Local>();
        localListAdapter = new LocalListAdapter(getApplicationContext(), R.layout.local_item, localList);
        localListView.setAdapter(localListAdapter);




        localListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Local listItem = (Local)localListView.getItemAtPosition(position);
                //Toast.makeText(EscolherlocalActivity.this, listItem.getNome(),Toast.LENGTH_LONG).show();
                // Passar a info do ID.
                Intent intent = new Intent();
                intent.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_ID_AS_INT, listItem.getId());
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });

        updateMyList();

    }


    public void updateMyList(){
        localList.clear();
        //IPersistencia db = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        localList.addAll(bd.getAllLocal());
        localListAdapter.notifyDataSetChanged();
    }

}
