package simov.isep.ondeandas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import simov.isep.ondeandas.model.Local;

public class LocalListAdapter extends BaseAdapter {

    private final List<Local> items;

    public LocalListAdapter(final Context context, final int itemResId, final List<Local> items) {
        this.items = items;
    }

    public int getCount() {
        return this.items.size();
    }


    public Object getItem(int arg0) {
        return this.items.get(arg0);
    }


    public long getItemId(int arg0) {
        return 0;
    }


    public View getView(int arg0, View arg1, ViewGroup arg2) {
        final Local row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.local_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row
        TextView txtId = (TextView) itemView.findViewById(R.id.local_Id);
        txtId.setText(Integer.toString(row.getId()));

        TextView txtTitle = (TextView) itemView.findViewById(R.id.local_Title);
        txtTitle.setText(row.getNome());



        return itemView;
    }

}
