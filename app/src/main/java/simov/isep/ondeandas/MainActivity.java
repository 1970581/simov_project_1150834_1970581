package simov.isep.ondeandas;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.support.annotation.NonNull;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import simov.isep.ondeandas.model.Local;
import simov.isep.ondeandas.notificacoes.EnviaNotificacoes;
import simov.isep.ondeandas.persistencia.AjudaPersistencia;
import simov.isep.ondeandas.persistencia.IPersistencia;
import simov.isep.ondeandas.persistencia.MemoriaLogs;
import simov.isep.ondeandas.servicolocalizacao.ConstantesLocalizacao;
import simov.isep.ondeandas.servicolocalizacao.GeofenceTransitionsIntentService;
import simov.isep.ondeandas.servicolocalizacao.GeofencerOAService;

public class MainActivity extends AppCompatActivity {

    // Nome da class
    private static final String TAG = "MainActivity";
    private static final String MY_SPACER = "****************************************************** ";
    static final int REQUEST_CODE_GESTAO = 1;
    static final int REQUEST_CODE_VER_LOGS = 2;
    static final int REQUEST_CODE_ENVIAR_SMS = 3;
    static final int REQUEST_CODE_ENVIAR_FIREBASE = 4;


    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    static final String[] myPermitionArray = { Manifest.permission.ACCESS_FINE_LOCATION };


    //Geofencing
    private GeofencingClient mGeofencingClient;
    private List<Geofence> mGeofenceList;
    public PendingIntent mGeofencePendingIntent;
    public boolean isGeofenceActive = false;

    // Buttoes
    Button butao_restart_geo;
    Button butao_stop_geo;
    Button butao_logs;
    Button button_sms;
    Button button_teste_firebase;

    // Broadcast receiver, para receber info do Servico GeofencerOAService. Tem de iniciar no on create.
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(GeofencerOAService.INTENT_FILTER_ACTION_GEO_SERVICE_STARTED)){
                //if(b2 !=null) b2.setTextColor(Color.YELLOW);
                setIsGeofenceActiveTrue();
            }
            if (intent.getAction().equals(GeofencerOAService.INTENT_FILTER_ACTION_GEO_SERVICE_STOPED)){
                setIsGeofenceActiveFalse();
            }


        }
    };

    // Ligacoes do bound SERVICE GeofencerOAService
    private GeofencerOAService service;
    private Boolean isBound=false;
    private ServiceConnection mConnection

            = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder) {
            //Toast.makeText(MainActivity.this, "MainActivity:onServiceConnected",Toast.LENGTH_SHORT).show();
            service = ((GeofencerOAService.MyBinder) binder).getService();
            isBound = true;
            //if(service != null) checkIfGeoIsStarted();
            //if(service != null) service.startGeoService();

            Log.e(TAG, "**************************************************** " + "Main Service bounded.");
        }
        public void onServiceDisconnected(ComponentName className) {
            service = null;
            isBound = false;
            Log.e(TAG, "**************************************************** " + "Main Service is UNBOUNDED.");
        }

    };

    void doBind() {
        Intent intent = new Intent(MainActivity.this, GeofencerOAService.class);
        //Toast.makeText(MainActivity.this, "MainActivity:doBind",Toast.LENGTH_SHORT).show();
        Log.e(TAG, MY_SPACER + "MainActivity:doBind");
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

    }
    void doUnbind() {
        //Toast.makeText(MainActivity.this,  "MainActivity:doUnBind",Toast.LENGTH_SHORT).show();
        Log.e(TAG, MY_SPACER + "MainActivity:doUnBind");
        unbindService(mConnection);
    }



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Broadcast receiver start up.
        IntentFilter intentFilter= new IntentFilter(GeofencerOAService.INTENT_FILTER_ACTION_GEO_SERVICE_STARTED);
        intentFilter.addAction(GeofencerOAService.INTENT_FILTER_ACTION_GEO_SERVICE_STOPED);
        registerReceiver(broadcastReceiver,intentFilter);


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "getInstanceId failed", task.getException());
                            Toast.makeText(MainActivity.this, "getInstanceId failed", Toast.LENGTH_LONG).show();
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        // Log and toast
                        String msg = "token: " + token;
                        Log.e(TAG, msg);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
                        MemoriaLogs.addLog(msg);
                    }
                });
        FirebaseMessaging.getInstance().subscribeToTopic("ondeAndas").
                addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (!task.isSuccessful()) {
                    Log.e(TAG, "SubscribeToTopic Failed", task.getException());
                    Toast.makeText(MainActivity.this, "SubscribeToTopic Failed", Toast.LENGTH_LONG).show();
                    MemoriaLogs.addLog("SubscribeToTopic Failed");
                    return;
                }

                String msg = "Subscribed to ondeAndas topic";
                Log.d(TAG, msg);
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });


        Button b1 = (Button) findViewById(R.id.button1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                //Toast.makeText(MainActivity.this, "Gerir botao carregado", Toast.LENGTH_SHORT).show();
                Log.e(TAG, MY_SPACER + "MainActivity: Gerir botao carregado");

                //Intent de start activity for result, pk podemos necessitar de passar informação de volta.
                Intent i = new Intent(MainActivity.this, GestaoActivity.class);
                startActivityForResult(i,REQUEST_CODE_GESTAO);

            }
        });

        butao_restart_geo = (Button) findViewById(R.id.main_button_restart_geo);
        butao_restart_geo.setBackgroundColor(Color.RED);
        butao_restart_geo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.e(TAG, MY_SPACER + "MainActivity: botao restart carregado");
                startGeoServiceBound();

            }
        });

        butao_stop_geo = (Button) findViewById(R.id.main_button_stop_geo);
        butao_stop_geo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.e(TAG, MY_SPACER + "MainActivity: botao stop carregado");
                endGeoServiceBound();

            }
        });

        butao_logs = (Button) findViewById(R.id.main_button_Logs);
        butao_logs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.e(TAG, MY_SPACER + "MainActivity: botao logs carregado");
                Intent i = new Intent(MainActivity.this, VerlogActivity.class);
                startActivityForResult(i,REQUEST_CODE_VER_LOGS);
            }
        });

        button_sms = (Button) findViewById(R.id.main_button_sms);
        button_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.e(TAG, MY_SPACER + "MainActivity: botao logs carregado");
                Intent i = new Intent(MainActivity.this, EnviarsmsActivity.class);
                startActivityForResult(i,REQUEST_CODE_ENVIAR_SMS);
            }
        });

        //************************** TESTE DE NOTIFICACOES:
        button_teste_firebase = (Button) findViewById(R.id.main_button_SendNotificacao_firebase);
        button_teste_firebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Log.e(TAG, MY_SPACER + "MainActivity: botao send Notificacao Firebase carregado");
                //MemoriaLogs.addLog("Botao send Notificacao Firebase carregado");

                //String txt_notificacao = "Notificacao de Teste";
                //new EnviaNotificacoes().execute(txt_notificacao);

                Intent i = new Intent(MainActivity.this, SendnotafirebaseActivity.class);
                startActivityForResult(i,REQUEST_CODE_ENVIAR_FIREBASE);

            }
        });
        //button_teste_firebase.setVisibility(View.INVISIBLE);



        MemoriaLogs.addLog("Main started.");


        checkPermisoes();

        //Inicia o a criacao do servico.          startGeoServiceBound();
        doBind();

    }

    @Override
    protected void onResume() {
        checkIfGeoIsStarted();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        doUnbind();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case REQUEST_CODE_GESTAO: {


                // Metodo para activar reiniciar automaticamente os geofences ao voltar de gestao.
                if(ConstantesLocalizacao.requiresGeofenceUpdate){
                    Log.e(TAG, MY_SPACER + " Geofence Requere Update.");
                    MemoriaLogs.addLog("Geofence Requere Update");
                    ConstantesLocalizacao.requiresGeofenceUpdate = false;
                    if (isGeofenceActive) {
                        endGeoServiceBound();
                    }
                    startGeoServiceBound();
                }


                if(resultCode== Activity.RESULT_OK){

                }

            }
            break;



            default:
        }
    }












    //==================================================
    //         PARTE DO GEOFENCING
    //==================================================

    /*
    public void startGeoService(){
        mGeofencingClient = LocationServices.getGeofencingClient(this);

        createGeofencesInList();

        if(mGeofenceList == null || mGeofenceList.isEmpty()) {

            String msgnofence = getApplicationContext().getString(R.string.nome_no_locations);//"Lista de Geofence vazia. Cria Locais.";
            Toast.makeText(MainActivity.this, msgnofence, Toast.LENGTH_LONG).show();
            MemoriaLogs.addLog(msgnofence);
            return;
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            String mesage002 = getApplicationContext().getString(R.string.nome_necessita_permicoes_localizacao);
            Toast.makeText(getApplicationContext(), mesage002, Toast.LENGTH_LONG).show();
            //return;
        }
        else {
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            // Geofences added
                            // ...
                            String mesg04 = getApplicationContext().getString(R.string.nome_geofences_added);
                            Log.i(TAG, MY_SPACER + mesg04);
                            Toast.makeText(getApplicationContext(), mesg04, Toast.LENGTH_SHORT).show();
                            setIsGeofenceActiveTrue();
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Failed to add geofences
                            // ...
                            String mesg05 = getApplicationContext().getString(R.string.nome_geofences_added_fail);
                            Toast.makeText(getApplicationContext(), mesg05, Toast.LENGTH_SHORT).show();
                            isGeofenceActive = false;
                            setIsGeofenceActiveFalse();
                        }
                    });

        }

    }

    private void createGeofencesInList(){
        mGeofenceList = new ArrayList<Geofence>();

        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        List<Local> myLocalList = bd.getAllLocal();

        for (Local l : myLocalList){
            try{
                mGeofenceList.add(new Geofence.Builder()
                        .setRequestId(" " + l.getId() + " " + l.getNome())
                        .setCircularRegion(l.getLat(), l.getLng(), l.getRaio())
                        .setExpirationDuration(ConstantesLocalizacao.tempoExpiracaoDuracaoDasGeoGences)//Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                        .setTransitionTypes(
                                Geofence.GEOFENCE_TRANSITION_ENTER |
                                Geofence.GEOFENCE_TRANSITION_DWELL |
                                Geofence.GEOFENCE_TRANSITION_EXIT)
                        .setLoiteringDelay(ConstantesLocalizacao.tempoParaDelayDeLoitering)
                        .build());
                Log.i(TAG, MY_SPACER + "GeoFence Criada");
            }
            catch(Exception ex){
                Log.i(TAG, MY_SPACER + "GeoFence Erro a criar " + ex.getMessage());
                MemoriaLogs.addLog("Create Fence error: " + ex.getMessage());
                throw ex;
            }
        }

    }


    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        //builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER |  GeofencingRequest.INITIAL_TRIGGER_EXIT);
        // Vamos so fazer trigger inicia se entrarmos ou ja nos encontrarmos no local.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    public void endGeoService(){
        mGeofencingClient.removeGeofences(getGeofencePendingIntent())
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences removed
                        // ...
                        setIsGeofenceActiveFalse();
                        String mesg07 = getApplicationContext().getString(R.string.nome_geofence_removed);
                        Toast.makeText(getApplicationContext(), mesg07, Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to remove geofences
                        // ...
                        String mesg08 = getApplicationContext().getString(R.string.nome_geofence_removed_fail);
                        Log.i(TAG, MY_SPACER + mesg08 );
                        Toast.makeText(getApplicationContext(), mesg08, Toast.LENGTH_SHORT).show();
                    }
                });
    }
    */

    public void setIsGeofenceActiveTrue(){
        this.isGeofenceActive = true;
        butao_restart_geo.setBackgroundColor(Color.GREEN);
        //MemoriaLogs.addLog("Geofenceing Active.");
    }

    public void setIsGeofenceActiveFalse(){
        this.isGeofenceActive = false;
        butao_restart_geo.setBackgroundColor(Color.RED);
        //MemoriaLogs.addLog("Geofenceing Stoped.");
    }

    public void startGeoServiceBound(){
        if(service != null) service.startGeoService();
    }
    public void endGeoServiceBound(){
        if(service != null) service.endGeoService ();
    }

    public void checkIfGeoIsStarted(){
        if(service != null){
            if(service.isStarted){
                setIsGeofenceActiveTrue();
            }
        }
    }

    //================================================== termina GEOFENCING


    //https://developer.android.com/training/permissions/requesting#java
    public void checkPermisoes(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            String mesage001 = getApplicationContext().getString(R.string.nome_necessita_permicoes_localizacao);
            Toast.makeText(getApplicationContext(), mesage001, Toast.LENGTH_LONG).show();

            //ActivityCompat.requestPermissions s(this,  arrayOf(Manifest.permission.READ_CONTACTS), MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            ActivityCompat.requestPermissions(this,  myPermitionArray, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        //startGeoService();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the contacts-related task you need to do.
                    Toast.makeText(getApplicationContext(), "PERMITIONS REQUEST GRANTED", Toast.LENGTH_LONG).show();
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(), "PERMITIONS REQUEST DENIED", Toast.LENGTH_LONG).show();
                }
                return; }
            // other 'case' lines to check for other permissions this app might request.
        }
    }

}
