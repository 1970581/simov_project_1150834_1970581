package simov.isep.ondeandas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GestaoActivity extends AppCompatActivity {

    // Nome da class
    private static final String TAG = "GestaoActivity";
    static final int REQUEST_CODE_GESTAO_LOCAL = 1;
    static final int REQUEST_CODE_GESTAO_HORARIO = 2;

    Button button_pai;
    Button button_filho;
    EditText editText_pai_phone;
    EditText editText_username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestao);

        //Gerir locais
        Button b1 = (Button) findViewById(R.id.button1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //Toast.makeText(GestaoActivity.this, "butao1", Toast.LENGTH_SHORT).show();
                Log.i(TAG, TAG + " botao 1 carregado");

                //Intent de start activity for result, pk podemos necessitar de passar informação de volta.
                Intent i = new Intent(GestaoActivity.this, GestaolocaisActivity.class);
                startActivityForResult(i,REQUEST_CODE_GESTAO_LOCAL);

            }
        });

        //Gerir Horario
        Button b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //Toast.makeText(GestaoActivity.this, "butao2", Toast.LENGTH_SHORT).show();
                Log.i(TAG, TAG + " botao 2 carregado");
                Intent i = new Intent(GestaoActivity.this, GestaohorarioActivity.class);
                startActivityForResult(i,REQUEST_CODE_GESTAO_HORARIO);

            }
        });

        button_pai= (Button) findViewById(R.id.activity_gestao_role_pai);
        button_pai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setPai();
            }
        });

        button_filho= (Button) findViewById(R.id.activity_gestao_role_filho);
        button_filho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setFilho();
            }
        });

        editText_pai_phone = (EditText) findViewById(R.id.activity_gestao_numero_edittext);
        editText_username = (EditText) findViewById(R.id.activity_gestao_nome_edittext);

    }


    @Override
    protected void onStart() {
        super.onStart();
        //obterCampos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        obterCampos();
    }

    @Override
    protected void onPause() {
        salvarCampos();
        super.onPause();
    }

    @Override
    protected void onStop() {
        salvarCampos();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        salvarCampos();
        super.onDestroy();
    }



    public void salvarCampos(){
        //Context context = getActivity();
        //SharedPreferences sharedPref = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(LogicaUi.SHARED_PREFERENCE_FIELD_PHONE, editText_pai_phone.getText().toString());
        editor.putString(LogicaUi.SHARED_PREFERENCE_FIELD_USERNAME, editText_username.getText().toString());
        editor.commit();
    }
    public void obterCampos(){
        //Context context = getActivity();
        //SharedPreferences sharedPref = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        String readPaiPhone = sharedPref.getString(LogicaUi.SHARED_PREFERENCE_FIELD_PHONE,"");
        String readMyUsername = sharedPref.getString(LogicaUi.SHARED_PREFERENCE_FIELD_USERNAME,"");
        editText_pai_phone.setText(readPaiPhone);
        editText_username.setText(readMyUsername);
        updatePaiFilhoButton();
    }

    public void setPai(){
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(LogicaUi.SHARED_PREFERENCE_FIELD_ROLE_IS_PAI, true);
        editor.commit();
        if(button_pai != null) button_pai.setBackgroundColor(Color.GREEN);
        if(button_filho != null) button_filho.setBackgroundColor(Color.GRAY);
    }

    public void setFilho(){
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(LogicaUi.SHARED_PREFERENCE_FIELD_ROLE_IS_PAI, false);
        editor.commit();
        if(button_pai != null) button_pai.setBackgroundColor(Color.GRAY);
        if(button_filho != null) button_filho.setBackgroundColor(Color.GREEN);
    }

    public boolean isPai(){
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        boolean isPaiRole = sharedPref.getBoolean(LogicaUi.SHARED_PREFERENCE_FIELD_ROLE_IS_PAI,false);
        return isPaiRole;
    }

    public void updatePaiFilhoButton(){
        if(isPai()){
            if(button_pai != null) button_pai.setBackgroundColor(Color.GREEN);
            if(button_filho != null) button_filho.setBackgroundColor(Color.GRAY);
        }
        else{
            if(button_pai != null) button_pai.setBackgroundColor(Color.GRAY);
            if(button_filho != null) button_filho.setBackgroundColor(Color.GREEN);
        }
    }

}
