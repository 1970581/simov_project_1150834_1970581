package simov.isep.ondeandas;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import simov.isep.ondeandas.model.Horario;
import simov.isep.ondeandas.model.Local;
import simov.isep.ondeandas.persistencia.AjudaPersistencia;
import simov.isep.ondeandas.persistencia.IPersistencia;
import simov.isep.ondeandas.persistencia.MemoriaPersistencia;

public class GestaolocaisActivity extends AppCompatActivity {

    private static final String TAG = "GestaolocaisActivity";
    //private static final int REQUEST_CODE_CRIAR_LOCAL_INTENT = 31;
    //private static final int REQUEST_CODE_CRIAR_LOCAL_SUCESSO = 32;
    //private static final int REQUEST_CODE_CRIAR_LOCAL_FAIL = 33;

    static ListView localListView;
    ArrayList<Local> localList;
    private LocalListAdapter localListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestaolocais);

        // Lista de locais, e a sua View+Adapter
        localListView = (ListView) findViewById(R.id.listView);
        localList = new ArrayList<Local>();
        localListAdapter = new LocalListAdapter(getApplicationContext(), R.layout.local_item, localList);
        localListView.setAdapter(localListAdapter);

        //Menu de contexto. Long Press nos items.
        registerForContextMenu(localListView);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_local_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ol_add:
                //Toast.makeText(GestaolocaisActivity.this, "Add",Toast.LENGTH_LONG).show();

                // ==============================================================
                // Codigo para iniciar actividade de editar como criar novo local.
                Intent i=new Intent(this,EditarlocalActivity.class);
                //i.putExtra(getString(R.string.operation_type),getString(R.string.operation_add));
                i.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_OPERATION, LogicaUi.INTENT_EXTRA_STRING_CREATE_NEW_LOCAL);
                startActivityForResult(i,LogicaUi.UI_LOCAL_REQUEST_CODE_CREATE_NEW_LOCAL);

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_local_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        Intent intent;
        Local myLocal;

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position_id = info.position;
        switch(item.getItemId()){
            case R.id.lc_delete:
                myLocal = (Local)localListAdapter.getItem(position_id);

                //Toast.makeText(GestaolocaisActivity.this, "context_delete " + myLocal.getNome(), Toast.LENGTH_SHORT).show();
                Log.i(TAG, TAG + " context delete " + myLocal.getNome() );

                deleteLocal(myLocal);
                updateMyList();

                position_id= -1;
                break;
            case R.id.lc_edit:

                //Toast.makeText(GestaolocaisActivity.this, "context_edit " + myLocal.getNome(), Toast.LENGTH_SHORT).show();
                myLocal = (Local)localListAdapter.getItem(position_id);
                Log.i(TAG, TAG + " context edit " + myLocal.getNome() + " id local: " + myLocal.getId());

                // ==============================================================
                // Codigo para iniciar actividade de editar como editar local.
                Intent i=new Intent(this,EditarlocalActivity.class);
                i.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_OPERATION, LogicaUi.INTENT_EXTRA_STRING_EDIT_LOCAL);
                i.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_ID_AS_INT, myLocal.getId());
                startActivityForResult(i,LogicaUi.UI_LOCAL_REQUEST_CODE_EDIT_LOCAL);


                position_id= -1;
                break;

            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }


    // ACTIVITY RETURN - Quando fazes back de uma activity iniciada por esta activity.
    // Usada para fazer accoes conforme o codigo devolvido.

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case LogicaUi.UI_LOCAL_REQUEST_CODE_EDIT_LOCAL:
                if(resultCode== Activity.RESULT_OK){
                    updateMyList();

                }
                break;

            case LogicaUi.UI_LOCAL_REQUEST_CODE_CREATE_NEW_LOCAL:
                if(resultCode== Activity.RESULT_OK){
                    updateMyList();
                }
                break;

            default:
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        updateMyList();
    }

    public void updateMyList(){
        localList.clear();
        //IPersistencia db = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        localList.addAll(bd.getAllLocal());
        localListAdapter.notifyDataSetChanged();
    }

    public boolean deleteLocal(Local localApagar){
        //IPersistencia db = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());

        // Vamos confirmar que um horario nao esta a usar aquele local. Se tiver, negamos.
        List<Horario> todosHorarios = bd.getAllHorario();
        int idDoLocalApagar = localApagar.getId();

        boolean existe = false;
        for(Horario h : todosHorarios){
            if (h.getIdLocal() == idDoLocalApagar) existe = true;
        }
        if(existe){
            String message1 = getApplicationContext().getString(R.string.nome_local_existe_em_horario);
            Toast.makeText(GestaolocaisActivity.this, message1 , Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean sucesso = bd.apgarLocal(localApagar);
        Log.i(TAG, TAG + " context delete " + sucesso + " "+ localApagar.getNome() );
        return sucesso;
    }
}
