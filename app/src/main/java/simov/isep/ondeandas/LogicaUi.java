package simov.isep.ondeandas;

public abstract class LogicaUi {

    // Codigos para usar no StartActivity for Result, para o Request code.
    public final static int UI_LOCAL_REQUEST_CODE_CREATE_NEW_LOCAL = 1000;
    public final static int UI_LOCAL_REQUEST_CODE_EDIT_LOCAL = 1001;
    public final static int UI_LOCAL_REQUEST_CODE_CREATE_NEW_HORARIO = 2000;
    public final static int UI_LOCAL_REQUEST_CODE_EDIT_HORARIO = 2001;
    public final static int UI_REQUEST_CODE_ESCOLHER_LOCAL = 3000;

    public final static int UI_LOCAL_REQUEST_CODE_OBTER_COORDENADAS = 4000;

    //Campos para usar no intent Extra, para enviar e receber informação.

    // Nome do campo do tipo operacao de criar ou editar:
    public final static String INTENT_EXTRA_STRING_CODE_TYPE_OPERATION = "code_type_operation";
    // valor do campo que indica criar local
    public final static String INTENT_EXTRA_STRING_CREATE_NEW_LOCAL = "create_local";
    // valor do campo que indica editar local
    public final static String INTENT_EXTRA_STRING_EDIT_LOCAL = "edit_local";
    // valor do campo que indica criar local
    public final static String INTENT_EXTRA_STRING_CREATE_NEW_HORARIO = "create_horario";
    // valor do campo que indica editar local
    public final static String INTENT_EXTRA_STRING_EDIT_HORARIO = "edit_horario";

    // nome do campo do tipo ID do local
    public final static String INTENT_EXTRA_STRING_CODE_TYPE_ID_AS_INT = "id_as_int";

    // nome do campo para obter coordenadas para o parametro Latitude e Longitude no intent.
    public final static String INTENT_EXTRA_STRING_CODE_TYPE_LATITUDE_AS_DOUBLE = "lat_as_double";
    public final static String INTENT_EXTRA_STRING_CODE_TYPE_LONGITUDE_AS_DOUBLE = "lng_as_double";

    //Shared preferences, um ficheiro na memoria do telemovel. Serve para presistir coisas basicas.
    public static final String NAME_OF_SHARED_PREFERENCE_FILE = "ONDE_ANDAS_SP";

    public static final String SHARED_PREFERENCE_FIELD_PHONE = "SHARED_PREFERENCE_FIELD_PHONE";
    public static final String SHARED_PREFERENCE_FIELD_MESSAGE = "SHARED_PREFERENCE_FIELD_MESSAGE";
    public static final String SHARED_PREFERENCE_FIELD_USERNAME = "SHARED_PREFERENCE_FIELD_USERNAME";
    public static final String SHARED_PREFERENCE_FIELD_ROLE_IS_PAI = "SHARED_PREFERENCE_FIELD_ROLE_IS_PAI";
    //public static final String SHARED_PREFERENCE_FIELD_ROLE_RESULT_PAI = "PAI";
    //public static final String SHARED_PREFERENCE_FIELD_ROLE_RESULT_FILHO = "FILHO";

    public static final String SHARED_PREFERENCE_FIELD_MESSAGE_FIREBASE = "SHARED_PREFERENCE_FIELD_MESSAGE_FIREBASE";

}
