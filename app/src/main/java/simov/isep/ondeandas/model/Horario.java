package simov.isep.ondeandas.model;

import android.content.Context;
import android.support.annotation.NonNull;

import simov.isep.ondeandas.R;

public class Horario implements Comparable<Horario>{

    public final static int MAX_NAME_LENGHT = 60;

    public int Id;

    public String nome;

    public int diaSemana;

    public int horaInicio;

    public int minutoInicio;

    public int horaFim;

    public int minutoFim;

    public int idLocal;

    public Horario(){}

    public int getId() {                return Id;     }
    public String getNome(){            return nome;}
    public int getDiaSemana() {         return diaSemana;    }
    public int getHoraInicio() {        return horaInicio;    }
    public int getMinutoInicio() {      return minutoInicio;    }
    public int getHoraFim(){            return horaFim;}
    public int getMinutoFim() {         return minutoFim;    }
    public int getIdLocal() {           return idLocal;    }

    public void setId(int id) {                     this.Id = id;    }
    public void setNome(String nome) {              this.nome = nome;    }
    public void setDiaSemana(int diaSemana) {       this.diaSemana = diaSemana;    }
    public void setHoraInicio(int horaInicio) {     this.horaInicio = horaInicio;    }
    public void setMinutoInicio(int minutoInicio) { this.minutoInicio = minutoInicio;    }
    public void setHoraFim(int horaFim) {           this.horaFim = horaFim;    }
    public void setMinutoFim(int minutoFim) {       this.minutoFim = minutoFim;    }
    public void setIdLocal(int idLocal) {           this.idLocal = idLocal;    }




    public String getNomeDiaSemana(Context context){
        String retorno = "invalido";
        switch(this.diaSemana){
            case 1: retorno = context.getString(R.string.dia1); break;
            case 2: retorno = context.getString(R.string.dia2); break;
            case 3: retorno = context.getString(R.string.dia3); break;
            case 4: retorno = context.getString(R.string.dia4); break;
            case 5: retorno = context.getString(R.string.dia5); break;
            case 6: retorno = context.getString(R.string.dia6); break;
            case 7: retorno = context.getString(R.string.dia7); break;
            default: retorno = "Invalido"; break;
        }
        return retorno;
    }




    public String getTempoInicio(){
        //return "" + this.horaInicio + ":" + this.minutoInicio;
        return "" + String.format("%02d",this.horaInicio) + ":" + String.format("%02d",this.minutoInicio);
    }

    public String getTempoFim(){
        //return "" + this.horaFim + ":" + this.minutoFim;
        return "" + String.format("%02d", this.horaFim) + ":" + String.format("%02d", this.minutoFim);
    }


    public boolean validateNome(String nomeString){
        if(nomeString == null || nomeString.isEmpty() || nomeString.length() > MAX_NAME_LENGHT) return false;
        return true;
    }

    public boolean validateDiaSemana(String dia){
        if(dia == null || dia.isEmpty() ) return false;
        int myDia = -1;
        try{ myDia = Integer.parseInt( dia);  }
        catch (NumberFormatException ex){ return false; }
        if (myDia < 1 || myDia > 7) return false;
        return true;
    }

    public boolean validateHora(String hora){
        if(hora == null || hora.isEmpty() ) return false;
        int myHora = -1;
        try{ myHora = Integer.parseInt( hora);  }
        catch (NumberFormatException ex){ return false; }
        if (myHora < 0 || myHora > 23) return false;
        return true;
    }

    public boolean validateMinute(String minuto){
        if(minuto == null || minuto.isEmpty() ) return false;
        int myMinuto = -1;
        try{ myMinuto = Integer.parseInt( minuto);  }
        catch (NumberFormatException ex){ return false; }
        if (myMinuto < 0 || myMinuto > 59) return false;
        return true;
    }

    public boolean validateFimMaiorQueInicio(){
        if (this.horaInicio < 0 || this.horaInicio > 23) return false;
        if (this.horaFim < 0 || this.horaFim > 23) return false;
        if (this.minutoInicio < 0 || this.minutoInicio > 59) return false;
        if (this.minutoFim < 0 || this.minutoFim > 59) return false;
        int myInicio = this.horaInicio * 100 + this.minutoInicio;
        int myFim = this.horaFim * 100 + this.minutoFim;
        if (myFim <= myInicio) return false;
        return true;
    }

    public Horario clone(){
        Horario clone = new Horario();
        clone.Id = this.Id;
        clone.diaSemana = this.diaSemana;
        clone.nome = this.nome;
        clone.horaInicio = this.horaInicio;
        clone.horaFim = this.horaFim;
        clone.minutoInicio = this.minutoInicio;
        clone.minutoFim = this.minutoFim;
        clone.idLocal = this.idLocal;
        return clone;
    }

    public void resetToBlank(){
        this.Id = -1;
        this.diaSemana = 2;
        this.nome = "";
        horaInicio = 00;
        horaFim = 23;
        minutoInicio = 00;
        minutoFim = 55;
        idLocal = -1;
    }

    public void aumentarDiaSemana(){
        this.diaSemana++;
        if (diaSemana > 7) this.diaSemana = 1;
    }
    public void diminuirDiaSemana(){
        this.diaSemana--;
        if (diaSemana <1) this.diaSemana = 7;
    }

    public void aumentarHoraInicio(){
        this.horaInicio++;
        if (horaInicio > 23) horaInicio = 0;
    }
    public void diminuirHoraInicio(){
        this.horaInicio--;
        if (horaInicio <0) horaInicio = 23;
    }
    public void aumentarMinutoInicio(){
        this.minutoInicio = this.minutoInicio +5;
        if(minutoInicio > 59) minutoInicio = 0;
    }
    public void diminuirMinutoInicio(){
        this.minutoInicio = this.minutoInicio -5;
        if(minutoInicio < 0) minutoInicio = 55;
    }

    public void aumentarHoraFim(){
        this.horaFim++;
        if (horaFim > 23) horaFim = 0;
    }
    public void diminuirHoraFim(){
        this.horaFim--;
        if (horaFim < 0) horaFim = 23;
    }
    public void aumentarMinutoFim(){
        this.minutoFim = this.minutoFim +5;
        if(minutoFim > 59) minutoFim = 0;
    }
    public void diminuirMinutoFim(){
        this.minutoFim = this.minutoFim -5;
        if(minutoFim < 0) minutoFim = 55;
    }


    @Override
    public int compareTo(@NonNull Horario o) {
        // -1 se this menor que other
        if(this.diaSemana < o.diaSemana) return -1;
        if(this.diaSemana > o.diaSemana) return 1;
        if(this.horaInicio < o.horaInicio) return -1;
        if(this.horaInicio > o.horaInicio) return 1;
        if(this.minutoInicio < o.minutoInicio) return -1;
        if(this.minutoInicio > o.minutoInicio) return 1;
        if(this.horaFim < o.horaFim) return -1;
        if(this.horaFim > o.horaFim) return 1;
        if(this.minutoFim < o.minutoFim) return -1;
        if(this.minutoFim > o.minutoFim) return 1;
        if(this.idLocal < o.idLocal) return -1;
        if(this.idLocal > o.idLocal) return 1;
        return o.Id - this.Id;
    }
}
