package simov.isep.ondeandas;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import simov.isep.ondeandas.model.Local;
import simov.isep.ondeandas.persistencia.AjudaPersistencia;
import simov.isep.ondeandas.persistencia.IPersistencia;
import simov.isep.ondeandas.persistencia.MemoriaPersistencia;
import simov.isep.ondeandas.servicolocalizacao.ConstantesLocalizacao;

public class EditarlocalActivity extends AppCompatActivity {

    private static final String TAG = "EditarlocalActivity";
    // id do local, -1 se for criar um novo.
    int idLocal;
    TextView textView_id;
    EditText editText_nome;
    EditText editText_lat;
    EditText editText_lng;
    EditText editText_raio;

    String operationType;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editarlocal);

        Intent data = getIntent();
        operationType = data.getStringExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_OPERATION );
        idLocal = data.getIntExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_ID_AS_INT,-1);

        textView_id = (TextView) findViewById(R.id.editarlocal_textView_id);
        editText_nome = (EditText) findViewById(R.id.editarlocal_editText_nome);
        editText_lat = (EditText) findViewById(R.id.editarlocal_editText_latitude);
        editText_lng = (EditText) findViewById(R.id.editarlocal_editText_longitude);
        editText_raio = (EditText) findViewById(R.id.editarlocal_editText_raio);


        //========================
        //      VALIDACOES
        //========================

        editText_nome.addTextChangedListener( new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {  }
                @Override
                public void afterTextChanged(Editable s) {
                    boolean valida = new Local().validateNome(editText_nome.getText().toString());
                    if (!valida){
                        Toast.makeText(EditarlocalActivity.this, "Nome Invalido", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        );

        editText_lat.addTextChangedListener( new TextWatcher() {

                                                  @Override
                                                  public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                                                  @Override
                                                  public void onTextChanged(CharSequence s, int start, int before, int count) {  }
                                                  @Override
                                                  public void afterTextChanged(Editable s) {
                                                      boolean valida = new Local().validateLat(editText_lat.getText().toString());
                                                      if (!valida){
                                                          Toast.makeText(EditarlocalActivity.this, "Latitude Invalido", Toast.LENGTH_SHORT).show();
                                                      }
                                                  }
                                              }
        );

        editText_lng.addTextChangedListener( new TextWatcher() {

                                                 @Override
                                                 public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                                                 @Override
                                                 public void onTextChanged(CharSequence s, int start, int before, int count) {  }
                                                 @Override
                                                 public void afterTextChanged(Editable s) {
                                                     boolean valida = new Local().validateLng(editText_lng.getText().toString());
                                                     if (!valida){
                                                         Toast.makeText(EditarlocalActivity.this, "Longitude Invalido", Toast.LENGTH_SHORT).show();
                                                     }
                                                 }
                                             }
        );

        editText_raio.addTextChangedListener( new TextWatcher() {

                                                  @Override
                                                  public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                                                  @Override
                                                  public void onTextChanged(CharSequence s, int start, int before, int count) {  }
                                                  @Override
                                                  public void afterTextChanged(Editable s) {
                                                      boolean valida = new Local().validateRaio(editText_raio.getText().toString());
                                                      if (!valida){
                                                          Toast.makeText(EditarlocalActivity.this, "Raio Invalido", Toast.LENGTH_SHORT).show();
                                                      }
                                                  }
                                              }
        );


        // ======================
        //       Botoes.
        // ======================

        Button bt_save = (Button) findViewById(R.id.editarlocal_button_save);
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_CREATE_NEW_LOCAL)){
                    Local novoLocal = new Local();
                    String nome = editText_nome.getText().toString();
                    String lat = editText_lat.getText().toString();
                    String lng = editText_lng.getText().toString();
                    String raio = editText_raio.getText().toString();
                    boolean valida = novoLocal.setDados( nome, lat, lng, raio);

                    if (valida){
                        //IPersistencia bd = new MemoriaPersistencia();
                        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
                        bd.criarLocal(novoLocal);
                        ConstantesLocalizacao.requiresGeofenceUpdate = true;
                        Intent intent = new Intent();
                        setResult(Activity.RESULT_OK,intent);
                        finish();
                    }
                    else{
                        Toast.makeText(EditarlocalActivity.this, "Dados invalidos, aborted save/create.", Toast.LENGTH_SHORT).show();
                    }
                }

                if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_EDIT_LOCAL)){
                    //IPersistencia bd = new MemoriaPersistencia();
                    IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
                    Local novoLocal = bd.getLocalById(idLocal);
                    if(novoLocal == null){
                        Toast.makeText(EditarlocalActivity.this, "Local == null", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String nome = editText_nome.getText().toString();
                    String lat = editText_lat.getText().toString();
                    String lng = editText_lng.getText().toString();
                    String raio = editText_raio.getText().toString();
                    boolean valida = novoLocal.setDados( nome, lat, lng, raio);

                    if (valida){
                        bd.editarLocal(novoLocal);
                        ConstantesLocalizacao.requiresGeofenceUpdate = true;
                        Intent intent = new Intent();
                        setResult(Activity.RESULT_OK,intent);
                        finish();
                    }
                    else{
                        Toast.makeText(EditarlocalActivity.this, "Dados invalidos, aborted save/edit.", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        Button bt_obterCoordenadas = (Button) findViewById(R.id.editarlocal_button_obter);
        bt_obterCoordenadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent( EditarlocalActivity.this,ObtercoordenadasActivity.class);
                startActivityForResult(i,LogicaUi.UI_LOCAL_REQUEST_CODE_OBTER_COORDENADAS);

            }
        });

        if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_EDIT_LOCAL)){
            updateWithLocalWithId(this.idLocal);
        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        // Obsoleto. Nao faz sentido, visto escreve por cima do obter coordenadas.
        //if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_EDIT_LOCAL)){
        //    updateWithLocalWithId(this.idLocal);
        //}

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case LogicaUi.UI_LOCAL_REQUEST_CODE_OBTER_COORDENADAS:
                if(resultCode== Activity.RESULT_OK){
                    double newLatitude = 0;
                    double newLongitude = 0;
                    newLatitude = data.getDoubleExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_LATITUDE_AS_DOUBLE, 41.1776);
                    newLongitude = data.getDoubleExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_LONGITUDE_AS_DOUBLE, -8.6077);
                    editText_lat.setText( String.valueOf(newLatitude) );
                    editText_lng.setText( String.valueOf(newLongitude) );

                }
                break;
            default:
        }
    }


    public void updateWithLocalWithId(int myId){
        if (myId < 0) return;
        //IPersistencia bd = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());

        Local myLocal = bd.getLocalById(myId);
        if(myLocal == null){
            Toast.makeText(EditarlocalActivity.this, "Local == null", Toast.LENGTH_SHORT).show();
            return;
        }
        textView_id.setText(String.valueOf(myLocal.getId()));
        editText_nome.setText(myLocal.getNome());
        editText_lat.setText( String.valueOf(myLocal.getLat()) );
        editText_lng.setText( String.valueOf(myLocal.getLng()) );
        editText_raio.setText( String.valueOf(myLocal.getRaio()) );
    }

    // OBSOLETO.
    public void obterCoordenadas(){
        double setLat = 41.176945;
        double setLng =-8.608080 ;
        editText_lat.setText( String.valueOf(setLat) );
        editText_lng.setText( String.valueOf(setLng) );
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
