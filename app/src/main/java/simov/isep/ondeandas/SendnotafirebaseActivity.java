package simov.isep.ondeandas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import simov.isep.ondeandas.notificacoes.EnviaNotificacoes;

public class SendnotafirebaseActivity extends AppCompatActivity {

    private static final String TAG = "EnviarsmsActivity";
    private static final String MY_SPACER = "****************************************************** ";

    EditText editText_message;
    Button button_enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendnotafirebase);



        editText_message = (EditText)  findViewById(R.id.SendnotafirebaseActivity_mensagem_edittext);

        button_enviar = (Button) findViewById(R.id.SendnotafirebaseActivity_butao_enviar);
        button_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                String texto01 = editText_message.getText().toString();

                String date = " ";
                String readMyUsername = "  ";

                DateFormat df = new SimpleDateFormat("yyyy.MM.dd, HH:mm");
                date = df.format(Calendar.getInstance().getTime());
                SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
                readMyUsername = sharedPref.getString(LogicaUi.SHARED_PREFERENCE_FIELD_USERNAME,"???");

                String mensagem = date + " " + readMyUsername + " " + texto01;

                new EnviaNotificacoes().execute(mensagem);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        obterCampos();
    }

    @Override
    protected void onPause() {
        super.onPause();
        salvarCampos();
    }

    @Override
    protected void onStop() {
        super.onStop();
        salvarCampos();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        salvarCampos();
    }

    public void salvarCampos(){
        //Context context = getActivity();
        //SharedPreferences sharedPref = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(LogicaUi.SHARED_PREFERENCE_FIELD_MESSAGE_FIREBASE, editText_message.getText().toString());

        editor.commit();
    }
    public void obterCampos(){
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);

        String myMesage = sharedPref.getString(LogicaUi.SHARED_PREFERENCE_FIELD_MESSAGE_FIREBASE,"");
        editText_message.setText(myMesage);
    }
}
