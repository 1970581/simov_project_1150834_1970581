package simov.isep.ondeandas.persistencia;

import java.util.ArrayList;
import java.util.List;

import simov.isep.ondeandas.model.Horario;
import simov.isep.ondeandas.model.Local;

public class MemoriaPersistencia implements IPersistencia {



    public MemoriaPersistencia(){

        if(MemoriaSingletonDB.localList.isEmpty()){
            Local a = new Local();
            a.setId(1);
            a.setNome("Hugo");
            //41.189445, -8.702840
            a.setLat(41.189445);
            a.setLng(-8.702840);
            a.setRaio(100);

            Local b = new Local();
            b.setId(2);
            b.setNome("ISEP B409");
            //41.176945,-8.608080 DO prof.
            //41.177684, -8.607788 do ISEP b309
            b.setLat(41.177684);
            b.setLng(-8.607788);
            b.setRaio(100);

            Local c = new Local();
            c.setId(3);
            c.setNome("Tiago");

            //41.164189 -8.552041 Local do Tiago
            c.setLat(41.164189);
            c.setLng(-8.552041);
            c.setRaio(100);

            MemoriaSingletonDB.localList.add(a);
            MemoriaSingletonDB.localList.add(b);
            MemoriaSingletonDB.localList.add(c);
        }

        if(MemoriaSingletonDB.horarioList.isEmpty()){
            Horario h1 = new Horario();
            Horario h2 = new Horario();
            Horario h3 = new Horario();
            Horario h4 = new Horario();
            Horario h5 = new Horario();
            Horario h6 = new Horario();
            Horario h7 = new Horario();

            Horario h;

            h = h1;
            h.setDiaSemana(1);h.setNome("Domingo");
            h.setId(1);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);


            h = h2;
            h.setDiaSemana(2);h.setNome("Segunda");
            h.setId(2);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            h = h3;
            h.setDiaSemana(3);h.setNome("Terca");
            h.setId(3);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            h = h4;
            h.setDiaSemana(4);h.setNome("Quarta");
            h.setId(4);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            h = h5;
            h.setDiaSemana(5);h.setNome("Quinta");
            h.setId(5);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            h = h6;
            h.setDiaSemana(6);h.setNome("Sexta");
            h.setId(6);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            h = h7;
            h.setDiaSemana(7);h.setNome("Sabado");
            h.setId(7);h.setIdLocal(1);
            h.setHoraInicio(8);h.setMinutoInicio(0);
            h.setHoraFim(23);h.setMinutoFim(0);

            MemoriaSingletonDB.horarioList.add(h1);
            MemoriaSingletonDB.horarioList.add(h2);
            MemoriaSingletonDB.horarioList.add(h3);
            MemoriaSingletonDB.horarioList.add(h4);
            MemoriaSingletonDB.horarioList.add(h5);
            MemoriaSingletonDB.horarioList.add(h6);
            MemoriaSingletonDB.horarioList.add(h7);
        }


    }

    @Override
    public Local getLocalById(int id) {
        Local resposta = null;
        for (Local l : MemoriaSingletonDB.localList){
            if (l.getId() == id) resposta = l;
        }
        return resposta;
    }

    @Override
    public boolean criarLocal(Local novoLocal) {
        int myId = this.getNextLocalId();
        novoLocal.setId(myId);
        MemoriaSingletonDB.localList.add(novoLocal);
        return true;
    }

    @Override
    public boolean editarLocal(Local localAEditar) {
        int myId = localAEditar.getId();
        Local actual = this.getLocalById(myId);
        if(actual == null) return false;
        MemoriaSingletonDB.localList.remove(actual);
        MemoriaSingletonDB.localList.add(localAEditar);
        return true;
    }

    @Override
    public boolean apgarLocal(Local localAApagar) {
        int myId = localAApagar.getId();
        Local actual = this.getLocalById(myId);
        if(actual == null) return false;
        MemoriaSingletonDB.localList.remove(actual);
        return true;
    }

    @Override
    public List<Local> getAllLocal(){
        List<Local> locais = new ArrayList<Local>();
        for (Local l : MemoriaSingletonDB.localList){
            locais.add(l);
        }
        return locais;
    }

    //========================= HORARIOS


    @Override
    public Horario getHorarioById(int id) {
        Horario resposta = null;
        for (Horario h : MemoriaSingletonDB.horarioList){
            if (h.getId() == id) resposta = h;
        }
        return resposta;
    }

    @Override
    public boolean criarHorario(Horario novoHorario) {
        int myId = this.getNextHorarioId();
        novoHorario.setId(myId);
        MemoriaSingletonDB.horarioList.add(novoHorario);
        return true;
    }

    @Override
    public boolean editarHorario(Horario horarioAEditar) {
        int myId = horarioAEditar.getId();
        Horario actual = this.getHorarioById(myId);
        if(actual == null) return false;
        MemoriaSingletonDB.horarioList.remove(actual);
        MemoriaSingletonDB.horarioList.add(horarioAEditar);
        return true;
    }

    @Override
    public boolean apagarHorario(Horario horarioAApagar) {
        int myId = horarioAApagar.getId();
        Horario actual = this.getHorarioById(myId);
        if(actual == null) return false;
        MemoriaSingletonDB.horarioList.remove(actual);
        return true;
    }

    @Override
    public List<Horario> getAllHorario() {
        List<Horario> horarios = new ArrayList<Horario>();
        for (Horario obj : MemoriaSingletonDB.horarioList){
            horarios.add(obj.clone());
        }
        return horarios;
    }

    public int getNextHorarioId(){
        int nextId = 0;
        for (Horario h : MemoriaSingletonDB.horarioList){
            if (h.getId() > nextId) nextId = h.getId();
        }

        nextId++;
        return nextId;
    }

    public int getNextLocalId(){
        int nextId = 0;
        for (Local l : MemoriaSingletonDB.localList){
            if (l.getId() > nextId) nextId = l.getId();
        }

        nextId++;
        return nextId;
    }

}
