package simov.isep.ondeandas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import simov.isep.ondeandas.persistencia.MemoriaLogs;

public class VerlogActivity extends AppCompatActivity {

    ListView lv;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verlog);

        list = new ArrayList<String>();
        List<String> auxList = MemoriaLogs.getLogList();
        for(String s : auxList){
            list.add(s);
        }

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
        lv = (ListView) findViewById(R.id.verLog_listView);
        lv.setAdapter(adapter);



    }
}
