package simov.isep.ondeandas.persistencia;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MemoriaLogs {

    public static List<String> logList = new ArrayList<String>();


    public static void addLog(String myNewLog){
        DateFormat df = new SimpleDateFormat("yyyy.MM.dd, HH:mm");
        String date = df.format(Calendar.getInstance().getTime());
        logList.add(date + " " + myNewLog);
    }

    public static List<String>getLogList(){
        List<String> cloneList = new ArrayList<String>();
        for(String s : logList){
            cloneList.add(s.toString());
        }
        return cloneList;
    }


}
