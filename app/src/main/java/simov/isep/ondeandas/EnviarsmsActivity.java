package simov.isep.ondeandas;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import simov.isep.ondeandas.servicolocalizacao.SendSMSIntentService;

// baseado em: https://google-developer-training.gitbooks.io/android-developer-phone-sms-course/content/Lesson%202/2_p_sending_sms_messages.html
public class EnviarsmsActivity extends AppCompatActivity {

    private static final String TAG = "EnviarsmsActivity";
    private static final String MY_SPACER = "****************************************************** ";
   // private static final String SHARED_PREFERENCE_FIELD_PHONE = "SHARED_PREFERENCE_FIELD_PHONE";
    //private static final String SHARED_PREFERENCE_FIELD_MESSAGE = "SHARED_PREFERENCE_FIELD_MESSAGE";

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1;

    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    EditText editText_phone;
    EditText editText_message;
    Button button_send;
    Button button_send_internal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviarsms);

        editText_phone = (EditText)  findViewById(R.id.EnviarsmsActivity_numero_edittext);
        editText_message = (EditText)  findViewById(R.id.EnviarsmsActivity_mensagem_edittext);

        button_send = (Button) findViewById(R.id.EnviarsmsActivity_butao_enviar);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                // Use format with "smsto:" and phone number to create smsNumber.
                String smsNumber = String.format("smsto: %s",
                        editText_phone.getText().toString());

                String sms = editText_message.getText().toString();

                // Create the intent.
                Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
                // Set the data for the intent as the phone number.
                smsIntent.setData(Uri.parse(smsNumber));
                // Add the message (sms) with the key ("sms_body").
                smsIntent.putExtra("sms_body", sms);

                // If package resolves (target app installed), send intent.
                if (smsIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(smsIntent);
                } else {
                    Log.d(TAG, "Can't resolve app for ACTION_SENDTO Intent");
                }


                //Intent i = new Intent(MainActivity.this, EnviarsmsActivity.class);
                //startActivityForResult(i,REQUEST_CODE_ENVIAR_SMS);
            }
        });


        button_send_internal = (Button) findViewById(R.id.EnviarsmsActivity_butao_enviar_Interno);
        button_send_internal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                //smsSendMessageInternal();  // Morreu, passou a ser por intentService.
                smsSendMessageByIntentService();

            }
        });

        checkForSmsPermission();

    }


    public void smsSendMessageByIntentService(){
        Intent i= new Intent(EnviarsmsActivity.this, SendSMSIntentService.class);
        // potentially add data to the intent
        i.putExtra(SendSMSIntentService.INTENT_EXTRA_STRING_BODY_FIELD_OA, editText_message.getText().toString());
        i.putExtra(SendSMSIntentService.INTENT_EXTRA_STRING_NUMBER_FIELD_OA, editText_phone.getText().toString());

        startService(i);

    }


    // MORREU: SEM USO, PASSAMOS A USAR UM Intent Service.
    public void smsSendMessageInternal() {


        // Set the destination phone number to the string in editText.
        String destinationAddress = editText_phone.getText().toString();
        // Get the text of the sms message.

        checkForSmsPermission();

        String smsMessage = editText_message.getText().toString();

        // Set the service center address if needed, otherwise null.
        String scAddress = null;
        // Set pending intents to broadcast
        // when message sent and when delivered, or set to null.
        PendingIntent sentIntent = null, deliveryIntent = null;

        // Comentado, porque da excepções. Funciona, mas tem de ser mais trabalhado.
        /*
        // obtido em: https://stackoverflow.com/questions/4967448/show-compose-sms-view-in-android
        //PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
        //PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
        sentIntent = PendingIntent.getBroadcast(EnviarsmsActivity.this, 0, new Intent(SENT), 0);
        deliveryIntent = PendingIntent.getBroadcast(EnviarsmsActivity.this, 0, new Intent(DELIVERED), 0);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        // ---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {

                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));
*/



        // Use SmsManager.
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage
                (destinationAddress, scAddress, smsMessage,
                        sentIntent, deliveryIntent);

    }

    private void disableSmsButton() {
        //"SMS usage disabled"
        String msg01 = getApplicationContext().getString(R.string.nome_SMS_disabled);
        Toast.makeText(this, msg01, Toast.LENGTH_LONG).show();

        button_send_internal.setVisibility(View.INVISIBLE);
        //button_send_internal.setVisibility(View.INVISIBLE);
        //Button retryButton = (Button) findViewById(R.id.button_retry);
        //retryButton.setVisibility(View.VISIBLE);
    }

    private void enableSmsButton() {
        button_send_internal.setVisibility(View.VISIBLE);
    }

    private void checkForSmsPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS) !=
                PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, getString(R.string.nome_permission_not_granted));
            // Permission not yet granted. Use requestPermissions().
            // MY_PERMISSIONS_REQUEST_SEND_SMS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS},
                    MY_PERMISSIONS_REQUEST_SEND_SMS);
        } else {
            // Permission already granted. Enable the SMS button.
            enableSmsButton();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (permissions[0].equalsIgnoreCase
                        (Manifest.permission.SEND_SMS)
                        && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted. Enable sms button.
                    enableSmsButton();
                } else {
                    // Permission denied.
                    Log.d(TAG, getString(R.string.nome_failure_permission_sms));
                    Toast.makeText(this,
                            getString(R.string.nome_failure_permission_sms),
                            Toast.LENGTH_LONG).show();
                    // Disable the sms button.
                    disableSmsButton();
                }
            }
            // other 'case' lines to check for other permissions this app might request.
        }
    }




    @Override
    protected void onStart() {
        super.onStart();
        //obterCampos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        obterCampos();
    }

    @Override
    protected void onPause() {
        super.onPause();
        salvarCampos();
    }

    @Override
    protected void onStop() {
        super.onStop();
        salvarCampos();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        salvarCampos();
    }

    public void salvarCampos(){
        //Context context = getActivity();
        //SharedPreferences sharedPref = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(LogicaUi.SHARED_PREFERENCE_FIELD_MESSAGE, editText_message.getText().toString());
        editor.putString(LogicaUi.SHARED_PREFERENCE_FIELD_PHONE, editText_phone.getText().toString());
        editor.commit();
    }
    public void obterCampos(){
        //Context context = getActivity();
        //SharedPreferences sharedPref = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences sharedPref = getSharedPreferences(LogicaUi.NAME_OF_SHARED_PREFERENCE_FILE, MODE_PRIVATE);

        String myMesage = sharedPref.getString(LogicaUi.SHARED_PREFERENCE_FIELD_MESSAGE,"");
        String myPhone = sharedPref.getString(LogicaUi.SHARED_PREFERENCE_FIELD_PHONE,"");
        editText_message.setText(myMesage);
        editText_phone.setText(myPhone);
    }


}
