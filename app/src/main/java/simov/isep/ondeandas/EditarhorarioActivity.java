package simov.isep.ondeandas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import simov.isep.ondeandas.model.Horario;
import simov.isep.ondeandas.model.Local;
import simov.isep.ondeandas.persistencia.AjudaPersistencia;
import simov.isep.ondeandas.persistencia.IPersistencia;
import simov.isep.ondeandas.persistencia.MemoriaPersistencia;

public class EditarhorarioActivity extends AppCompatActivity {

    private static final String TAG = "EditarhorarioActivity";
    // id do horario, -1 se for criar um novo.
    int idHorario;
    TextView textView_EditarhorarioActivity_Horario_Id;
    EditText editText_EditarhorarioActivity_editText_nome;
    TextView textView_EditarhorarioActivity_TextView_DiaSemana;
    Button button_EditarhorarioActivity_Button_DiaMenos;
    Button button_EditarhorarioActivity_Buton_DiaMais;

    TextView textView_EditarhorarioActivity_TextView_TempoInicio;
    Button button_EditarhorarioActivity_Button_TempoInicioMenosHora;
    Button button_EditarhorarioActivity_Buton_TempoInicioMenosMinuto;
    Button button_EditarhorarioActivity_Buton_TempoInicioMaisHora;
    Button button_EditarhorarioActivity_Buton_TempoInicioMaisMinuto;

    TextView textView_EditarhorarioActivity_TextView_TempoFim;
    Button button_EditarhorarioActivity_Button_TempoFimMenosHora;
    Button button_EditarhorarioActivity_Buton_TempoFimMenosMinuto;
    Button button_EditarhorarioActivity_Buton_TempoFimMaisHora;
    Button button_EditarhorarioActivity_Buton_TempoFimMaisMinuto;

    TextView textView_EditarhorarioActivity_TextView_Local_Nome;
    Button button_EditarhorarioActivity_Buton_ObterLocal;
    Button button_EditarhorarioActivity_Buton_Salvar;

    String operationType;
    Horario horarioEditavel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editarhorario);

        Intent data = getIntent();
        operationType = data.getStringExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_OPERATION );
        idHorario = data.getIntExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_ID_AS_INT,-1);


        //CAIXAS:

        textView_EditarhorarioActivity_Horario_Id = (TextView) findViewById(R.id.EditarhorarioActivity_Horario_Id);
        editText_EditarhorarioActivity_editText_nome = (EditText) findViewById(R.id.EditarhorarioActivity_editText_nome);
        textView_EditarhorarioActivity_TextView_DiaSemana = (TextView) findViewById(R.id.EditarhorarioActivity_TextView_DiaSemana);

        textView_EditarhorarioActivity_TextView_TempoInicio = (TextView) findViewById(R.id.EditarhorarioActivity_TextView_TempoInicio);
        textView_EditarhorarioActivity_TextView_TempoFim = (TextView) findViewById(R.id.EditarhorarioActivity_TextView_TempoFim);

        textView_EditarhorarioActivity_TextView_Local_Nome = (TextView) findViewById(R.id.EditarhorarioActivity_TextView_Local_Nome);


        editText_EditarhorarioActivity_editText_nome.addTextChangedListener( new TextWatcher() {

                                                 @Override
                                                 public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                                                 @Override
                                                 public void onTextChanged(CharSequence s, int start, int before, int count) {  }
                                                 @Override
                                                 public void afterTextChanged(Editable s) {
                                                     horarioEditavel.setNome(editText_EditarhorarioActivity_editText_nome.getText().toString());
                                                     boolean valida = horarioEditavel.validateNome(horarioEditavel.getNome());
                                                     if (!valida){
                                                         Toast.makeText(EditarhorarioActivity.this, "Text to great", Toast.LENGTH_SHORT).show();
                                                     }
                                                 }
                                             }
        );


        // Butoes:

        button_EditarhorarioActivity_Button_DiaMenos = (Button) findViewById(R.id.EditarhorarioActivity_Button_DiaMenos);
        button_EditarhorarioActivity_Button_DiaMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.diminuirDiaSemana();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_DiaMais = (Button) findViewById(R.id.EditarhorarioActivity_Buton_DiaMais);
        button_EditarhorarioActivity_Buton_DiaMais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.aumentarDiaSemana();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Button_TempoInicioMenosHora = (Button) findViewById(R.id.EditarhorarioActivity_Button_TempoInicioMenosHora);
        button_EditarhorarioActivity_Button_TempoInicioMenosHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.diminuirHoraInicio();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_TempoInicioMenosMinuto = (Button) findViewById(R.id.EditarhorarioActivity_Buton_TempoInicioMenosMinuto);
        button_EditarhorarioActivity_Buton_TempoInicioMenosMinuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.diminuirMinutoInicio();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_TempoInicioMaisHora = (Button) findViewById(R.id.EditarhorarioActivity_Buton_TempoInicioMaisHora);
        button_EditarhorarioActivity_Buton_TempoInicioMaisHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.aumentarHoraInicio();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_TempoInicioMaisMinuto = (Button) findViewById(R.id.EditarhorarioActivity_Buton_TempoInicioMaisMinuto);
        button_EditarhorarioActivity_Buton_TempoInicioMaisMinuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.aumentarMinutoInicio();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Button_TempoFimMenosHora = (Button) findViewById(R.id.EditarhorarioActivity_Button_TempoFimMenosHora);
        button_EditarhorarioActivity_Button_TempoFimMenosHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.diminuirHoraFim();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_TempoFimMenosMinuto = (Button) findViewById(R.id.EditarhorarioActivity_Buton_TempoFimMenosMinuto);
        button_EditarhorarioActivity_Buton_TempoFimMenosMinuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.diminuirMinutoFim();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_TempoFimMaisHora = (Button) findViewById(R.id.EditarhorarioActivity_Buton_TempoFimMaisHora);
        button_EditarhorarioActivity_Buton_TempoFimMaisHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.aumentarHoraFim();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_TempoFimMaisMinuto = (Button) findViewById(R.id.EditarhorarioActivity_Buton_TempoFimMaisMinuto);
        button_EditarhorarioActivity_Buton_TempoFimMaisMinuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioEditavel.aumentarMinutoFim();
                doBasicUiUpdate();
            }
        });

        button_EditarhorarioActivity_Buton_ObterLocal = (Button) findViewById(R.id.EditarhorarioActivity_Buton_ObterLocal);
        button_EditarhorarioActivity_Buton_ObterLocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i2 = new Intent( EditarhorarioActivity.this, EscolherlocalActivity.class );
                startActivityForResult(i2,LogicaUi.UI_REQUEST_CODE_ESCOLHER_LOCAL);
            }
        });

        button_EditarhorarioActivity_Buton_Salvar = (Button) findViewById(R.id.EditarhorarioActivity_Buton_Salvar);
        button_EditarhorarioActivity_Buton_Salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateHorario()) return;
                if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_CREATE_NEW_HORARIO)){
                    boolean canCreate = saveCreateHorario();
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
                if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_EDIT_HORARIO)){
                    boolean canEdit = saveEditHorario();
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
            }
        });

        if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_CREATE_NEW_HORARIO)){
            horarioEditavel = new Horario();
            horarioEditavel.resetToBlank();
            doBasicUiUpdate();
        }
        if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_EDIT_HORARIO)){
            updateHorarioEditavelFromId(idHorario);
            doBasicUiUpdate();
            doLocalUiUpdate();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case LogicaUi.UI_REQUEST_CODE_ESCOLHER_LOCAL:
                if(resultCode== Activity.RESULT_OK){
                    int newIdLocal = -1;
                    newIdLocal = data.getIntExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_ID_AS_INT, -1);
                    horarioEditavel.setIdLocal(newIdLocal);
                    doLocalUiUpdate();
                }
                break;
            default:
        }
    }


    public void doBasicUiUpdate(){
        Context myContext = this.getApplicationContext();
        editText_EditarhorarioActivity_editText_nome.setText(horarioEditavel.getNome());
        textView_EditarhorarioActivity_TextView_DiaSemana.setText(horarioEditavel.getNomeDiaSemana(myContext));
        textView_EditarhorarioActivity_TextView_TempoInicio.setText(horarioEditavel.getTempoInicio());
        textView_EditarhorarioActivity_TextView_TempoFim.setText(horarioEditavel.getTempoFim());
    }

    public void doLocalUiUpdate(){
        Local theLocal = getLocalById(horarioEditavel.getIdLocal());
        if(theLocal != null) {        textView_EditarhorarioActivity_TextView_Local_Nome.setText(theLocal.getNome());}
        else{Toast.makeText(EditarhorarioActivity.this, "null Local", Toast.LENGTH_SHORT).show(); }
    }

    public Local getLocalById(int myIdLocal){
        //IPersistencia bd = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        Local local = bd.getLocalById(horarioEditavel.getIdLocal());
        return local;
    }


    public boolean validateHorario(){
        Context context = getApplicationContext();
        if(!horarioEditavel.validateFimMaiorQueInicio()){
            String message1 = context.getString(R.string.nome_horas_invalida);
            Toast.makeText(EditarhorarioActivity.this, message1, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!horarioEditavel.validateNome(horarioEditavel.getNome())){
            String message2 = context.getString(R.string.nome_horario_nome_to_long);
            Toast.makeText(EditarhorarioActivity.this, message2, Toast.LENGTH_SHORT).show();
            return false;
        }
        Local theLocal = getLocalById(horarioEditavel.getIdLocal());
        if(theLocal == null){
            String message3 = context.getString(R.string.nome_horario_local_invalido);
            Toast.makeText(EditarhorarioActivity.this, message3, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        if(operationType.equals(LogicaUi.INTENT_EXTRA_STRING_EDIT_HORARIO)){
            updateHorarioEditavelFromId(this.idHorario);
        }
        doBasicUiUpdate();
        */

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void updateHorarioEditavelFromId(int myId){
        if (myId < 0) return;
        //IPersistencia bd = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        horarioEditavel = bd.getHorarioById(myId).clone();
    }

    public boolean saveCreateHorario(){
        //IPersistencia bd = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        return bd.criarHorario(horarioEditavel);
    }

    public boolean saveEditHorario(){
        //IPersistencia bd = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
        return bd.editarHorario(horarioEditavel);
    }

}
