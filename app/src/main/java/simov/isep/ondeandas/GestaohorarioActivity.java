package simov.isep.ondeandas;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simov.isep.ondeandas.model.Horario;
import simov.isep.ondeandas.persistencia.AjudaPersistencia;
import simov.isep.ondeandas.persistencia.IPersistencia;
import simov.isep.ondeandas.persistencia.MemoriaPersistencia;

public class GestaohorarioActivity extends AppCompatActivity {

    private static final String TAG = "GestaohorarioActivity";
    static ListView horarioListView;
    ArrayList<Horario> horarioList;
    private HorarioListAdapter horarioListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestaohorario);

        // Lista de Horarios, e a sua View+Adapter
        horarioListView = (ListView) findViewById(R.id.activity_gestaohorario_listView);
        horarioList = new ArrayList<Horario>();
        horarioListAdapter = new HorarioListAdapter(getApplicationContext(), R.layout.horario_item, horarioList);
        horarioListView.setAdapter(horarioListAdapter);

        //Menu de contexto. Long Press nos items.
        registerForContextMenu(horarioListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_horario_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_options_horario_add:
                Toast.makeText(GestaohorarioActivity.this, "Add",Toast.LENGTH_LONG).show();

                // ==============================================================
                // Codigo para iniciar actividade de editar como criar novo horario.
                Intent i=new Intent(this,EditarhorarioActivity.class);

                i.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_OPERATION, LogicaUi.INTENT_EXTRA_STRING_CREATE_NEW_HORARIO);
                startActivityForResult(i,LogicaUi.UI_LOCAL_REQUEST_CODE_CREATE_NEW_HORARIO);

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_horario_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        Intent intent;
        Horario myHorario;

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position_id = info.position;
        switch(item.getItemId()){
            case R.id.menu_context_horario_delete:
                myHorario = (Horario) horarioListAdapter.getItem(position_id);

                //Toast.makeText(GestaolocaisActivity.this, "context_delete " + myLocal.getNome(), Toast.LENGTH_SHORT).show();
                Log.i(TAG, TAG + " context delete " + myHorario.getNome() );

                //IPersistencia db = new MemoriaPersistencia();
                IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());
                boolean sucesso = bd.apagarHorario(myHorario);
                Log.i(TAG, TAG + " context delete " + sucesso + " "+ myHorario.getNome() );
                updateMyList();

                position_id= -1;
                break;
            case R.id.menu_context_horario_edit:

                //Toast.makeText(GestaolocaisActivity.this, "context_edit " + myLocal.getNome(), Toast.LENGTH_SHORT).show();
                myHorario = (Horario) horarioListAdapter.getItem(position_id);
                Log.i(TAG, TAG + " context edit " + myHorario.getNome() + " id local: " + myHorario.getId());

                // ==============================================================
                // Codigo para iniciar actividade de editar como editar horario.
                Intent i=new Intent(this,EditarhorarioActivity.class);
                i.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_OPERATION, LogicaUi.INTENT_EXTRA_STRING_EDIT_HORARIO);
                i.putExtra(LogicaUi.INTENT_EXTRA_STRING_CODE_TYPE_ID_AS_INT, myHorario.getId());
                startActivityForResult(i,LogicaUi.UI_LOCAL_REQUEST_CODE_EDIT_HORARIO);


                position_id= -1;
                break;

            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case LogicaUi.UI_LOCAL_REQUEST_CODE_EDIT_HORARIO:
                if(resultCode== Activity.RESULT_OK){
                    updateMyList();

                }
                break;

            case LogicaUi.UI_LOCAL_REQUEST_CODE_CREATE_NEW_HORARIO:
                if(resultCode== Activity.RESULT_OK){
                    updateMyList();
                }
                break;

            default:
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateMyList();
    }

    public void updateMyList(){
        horarioList.clear();
        //IPersistencia db = new MemoriaPersistencia();
        IPersistencia bd = AjudaPersistencia.getDB(getApplicationContext());

        List<Horario> myList = new ArrayList<Horario>();
        List<Horario> dbList = bd.getAllHorario();

        for(Horario obj : dbList) {
            myList.add(obj.clone());
        }
        //myList. sort(null);
        Collections.sort(myList);
        horarioList.addAll(myList);

        horarioListAdapter.notifyDataSetChanged();
    }
}
