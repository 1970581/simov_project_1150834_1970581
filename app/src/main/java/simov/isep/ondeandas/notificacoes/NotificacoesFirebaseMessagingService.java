package simov.isep.ondeandas.notificacoes;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import simov.isep.ondeandas.GestaohorarioActivity;
import simov.isep.ondeandas.NotificationActivity;
import simov.isep.ondeandas.R;


public class NotificacoesFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String messageBody = "";
        Log.i(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        //messageBody = "Notification";
        //messageBody += "\n" + remoteMessage.getNotification().getTitle();
        //messageBody += "\n"+remoteMessage.getNotification().getBody();
        //messageBody += "\n Data";
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData().toString());
            //Toast.makeText(this, remoteMessage.getNotification().getBody(),Toast.LENGTH_LONG).show();
            messageBody += "\n" + remoteMessage.getData().get("body");

            //remoteMessage.getData().get("KEY").toString();

        }else{
            Log.d(TAG, "Emty Message");
        }
        sendNotification(messageBody);
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }

    private void sendNotification(String messageBody) {
       //Intent intent = new Intent(this, GestaohorarioActivity.class);
        Intent intent = new Intent(this, NotificationActivity.class);
        intent.putExtra("MESSAGE",messageBody);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "fcm_default_channel";
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.icon24x24)
                        .setContentTitle("Onde Andas")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(Long.signum(System.currentTimeMillis()) /* ID of notification */, notificationBuilder.build());

        //vibration code:
        try{
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                //deprecated in API 26
                v.vibrate(500);
            }
        }
        catch(Exception e){
            Log.e(TAG, "*************************************** Erro de vibracao: " + e.getMessage());
        }


    }
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

}
